package org.opencds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class TimingAbbreviationParser {
    public record FreqAbbrInfo(Integer low, Integer high, String units) {
    }

    static Map<String, String> timingPatterns = new LinkedHashMap<>();
    static Map<String, Integer> dailyFreqMap = new HashMap<>();

    static {
        timingPatterns.put("BIW", "\\b2x week\\b");
        timingPatterns.put("BID", "\\b2x\\b");
        timingPatterns.put("TID", "\\b3x\\b");
        timingPatterns.put("QID", "\\b4x\\b");
        timingPatterns.put("Q_N_H", "\\bq\\s?\\d+\\s?hr?\\b");
        timingPatterns.put("QHS", "\\bevery night\\b");
        timingPatterns.put("QAM", "\\bevery morning\\b");
        timingPatterns.put("Q2PM", "\\bq2pm\\b");
        timingPatterns.put("WK", "\\b1x week\\b");
        timingPatterns.put("QD", "\\bevery day\\b|\\b1x\\b|\\bq1d\\b");
        timingPatterns.put("QOD", "\\bevery other day\\b");

        dailyFreqMap.put("BID", 2);
        dailyFreqMap.put("TID", 3);
        dailyFreqMap.put("QID", 4);
        dailyFreqMap.put("QD", 1);
        dailyFreqMap.put("QHS", 1);
        dailyFreqMap.put("QAM", 1);
        dailyFreqMap.put("Q1H", 24);
        dailyFreqMap.put("Q2H", 12);
        dailyFreqMap.put("Q3H", 8);
        dailyFreqMap.put("Q4H", 6);
        dailyFreqMap.put("Q6H", 4);
        dailyFreqMap.put("Q8H", 3);
        dailyFreqMap.put("Q12H", 2);

    }

    /**
     * Extracts the timing abbreviation code from the normalized instruction string.
     * <p>
     * This method iterates through a map of known timing abbreviation codes
     * (<a href="https://hl7.org/fhir/valueset-timing-abbreviation.html">ValueSet timing abbreviation</a>) and
     * their corresponding regular expressions.
     * <p>
     * <p>
     * If no matching pattern is found, the method returns null.
     *
     * @return The timing abbreviation code extracted from the instruction string, or null if not found.
     */
    public static String getTimingAbbreviationCode(String normalizedSig) {

        if (hasMultipleAbbreviations(normalizedSig)) return null;

        for (Map.Entry<String, String> entry : timingPatterns.entrySet()) {
            String code = entry.getKey();
            String pattern = entry.getValue();
            var matcher = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE).matcher(normalizedSig);
            if (matcher.find()) {
                if (code.equals("Q_N_H")) {
                    return matcher.group()
                            .toUpperCase()
                            .replaceAll("\\s", "")
                            .replaceAll("R", "");
                }
                return code;
            }
        }

        return null; // not found
    }

    public static boolean hasMultipleAbbreviations(String normalizedSig) {
        List<String> commonAbbreviations = new ArrayList<>();
        commonAbbreviations.add("BID");
        commonAbbreviations.add("TID");
        commonAbbreviations.add("QID");
        commonAbbreviations.add("Q_N_H");
        commonAbbreviations.add("QHS");
        commonAbbreviations.add("Q2PM");
        commonAbbreviations.add("QAM");

        int matchCount = 0;

        for (String patternName : commonAbbreviations) {
            var matcher = Pattern.compile(timingPatterns.get(patternName), Pattern.CASE_INSENSITIVE).matcher(normalizedSig);
            if (matcher.find()) {
                matchCount++;
                if (matchCount > 1) {
                    return true;
                }
            }
        }

        return false;
    }

    public static FreqAbbrInfo resolveDailyFrequency(String abbr) {
        if (abbr == null) {
            return null;
        }

        Integer freq = dailyFreqMap.get(abbr);
        return freq != null ? new FreqAbbrInfo(freq, freq, "d") : null;
    }

}
