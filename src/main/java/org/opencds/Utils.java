package org.opencds;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.dstu2.model.BooleanType;
import org.hl7.fhir.dstu2.model.CodeableConcept;
import org.hl7.fhir.dstu2.model.Coding;
import org.hl7.fhir.dstu2.model.CommunicationRequest;
import org.hl7.fhir.dstu2.model.CommunicationRequest.CommunicationRequestPayloadComponent;
import org.hl7.fhir.dstu2.model.DateTimeType;
import org.hl7.fhir.dstu2.model.Duration;
import org.hl7.fhir.dstu2.model.Encounter;
import org.hl7.fhir.dstu2.model.Encounter.EncounterState;
import org.hl7.fhir.dstu2.model.HumanName;
import org.hl7.fhir.dstu2.model.Identifier;
import org.hl7.fhir.dstu2.model.Medication;
import org.hl7.fhir.dstu2.model.MedicationOrder;
import org.hl7.fhir.dstu2.model.MedicationOrder.MedicationOrderDispenseRequestComponent;
import org.hl7.fhir.dstu2.model.MedicationOrder.MedicationOrderDosageInstructionComponent;
import org.hl7.fhir.dstu2.model.Parameters;
import org.hl7.fhir.dstu2.model.Parameters.ParametersParameterComponent;
import org.hl7.fhir.dstu2.model.Patient;
import org.hl7.fhir.dstu2.model.Period;
import org.hl7.fhir.dstu2.model.Practitioner;
import org.hl7.fhir.dstu2.model.Range;
import org.hl7.fhir.dstu2.model.Reference;
import org.hl7.fhir.dstu2.model.Resource;
import org.hl7.fhir.dstu2.model.SimpleQuantity;
import org.hl7.fhir.dstu2.model.StringType;
import org.hl7.fhir.dstu2.model.Timing;
import org.hl7.fhir.dstu2.model.Timing.TimingRepeatComponent;
import org.hl7.fhir.dstu2.model.Timing.UnitsOfTime;
import org.hl7.fhir.dstu2.model.Type;
import org.opencds.common.utilities.AbsoluteTimeDifference;
import org.opencds.common.utilities.DateUtility;
import org.opencds.common.xml.XmlEntity;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

/**
 *
 *
 */
public class Utils {
    private static final Log log = LogFactory.getLog(Utils.class);

	public static boolean isMoreThan12hrs (Object dateTimeDt){
	    DateTimeType dateTime = (DateTimeType) dateTimeDt;

	    LocalDateTime startDateTime = LocalDateTime.ofInstant(dateTime.getValue().toInstant(), ZoneId.systemDefault());
	    LocalDateTime currentTime = LocalDateTime.now();
	    if (java.time.Duration.between(startDateTime, currentTime).toHours() > 12) {
	    	return true;
		}
	    return false;
    }

	/**
	 * Returns a Map that contains MedicationOrders mapped to the relevant Meds.
	 * Medications may be in meds OR contained in medicationOrder OR just a code, in which a new Medication with just a code will be created and used.
	 * @param medOrders
	 * @param meds
	 * @return
	 */
	public static Map<MedicationOrder, Medication> getMedOrderToAssociatedMedMap(List<MedicationOrder> medOrders, List<Medication> meds)
	{
		Map<MedicationOrder, Medication> medOrderToMed = new HashMap<MedicationOrder, Medication>();

		for (MedicationOrder medOrder : medOrders)
		{
			Medication associatedMed = Utils.getMedReferencedByMedOrder(medOrder, meds);
			if (associatedMed != null)
			{
				medOrderToMed.put(medOrder, associatedMed);
			}
			else
			{
				Medication containedMed = Utils.getMedContainedByMedOrder(medOrder);
				if (containedMed != null)
				{
					medOrderToMed.put(medOrder,  containedMed);
				}
				else
				{
					Medication impliedMed = Utils.getMedImpliedByMedOrderByCode(medOrder);
					if (impliedMed != null)
					{
						medOrderToMed.put(medOrder, impliedMed);
					}
				}
			}
		}
		return medOrderToMed;
	}

	/**
	 * Returns a Map that contains MedicationOrders mapped to the relevant prescribers (Practitioner type).
	 * Prescribers may be in practitioners OR contained in medicationOrder OR just a reference that has no practitoner entry (in at least some EHRs, this appears to happen for
	 * meds prescribed by "HISTORICAL, MEDS".  In the last case, if there is a display name, a new Practitioner with just the display name as the HumanName.text
	 * will be returned.
	 * @param medOrders
	 * @param practitioners
	 * @return
	 */
	public static Map<MedicationOrder, Practitioner> getMedOrderToAssociatedPrescriberMap(List<MedicationOrder> medOrders, List<Practitioner> practitioners)
	{
		Map<MedicationOrder, Practitioner> medOrderToPrescriber = new HashMap<MedicationOrder, Practitioner>();

		for (MedicationOrder medOrder : medOrders)
		{
			Practitioner associatedPrescriber = Utils.getPrescriberReferencedByMedOrder(medOrder, practitioners);
			if (associatedPrescriber != null)
			{
				medOrderToPrescriber.put(medOrder, associatedPrescriber);
			}
			else
			{
				Practitioner containedPrescriber = Utils.getPrescriberContainedByMedOrder(medOrder);
				if (containedPrescriber != null)
				{
					medOrderToPrescriber.put(medOrder,  containedPrescriber);
				}
				else
				{
					Practitioner impliedPrescriber = Utils.getPrescriberImpliedByMedOrderReferenceDisplay(medOrder);
					if (impliedPrescriber != null)
					{
						medOrderToPrescriber.put(medOrder, impliedPrescriber);
					}
				}
			}
		}
		return medOrderToPrescriber;
	}

	/**
	 * Returns medication contained in medOrder, or null if not found.
	 * @param medOrder
	 * @return
	 */
	private static Medication getMedContainedByMedOrder(MedicationOrder medOrder)
	{
		if (medOrder != null)
		{
			try
			{
				String medIdFromOrder = ((Reference) medOrder.getMedication()).getReference();
				if ((medIdFromOrder != null) && (medIdFromOrder.startsWith("#")))
				{
					List<Resource> contained = medOrder.getContained();
					for (Resource containedResource : contained)
					{
						if (containedResource instanceof Medication)
						{
							String containedResourceId = containedResource.getId();
							if ((containedResourceId != null) && (containedResourceId.equals(medIdFromOrder)))
							{
								return (Medication) containedResource;
							}
						}
					}
				}
			}
			catch(Exception e)
			{
				// do nothing; may be an expected outcome.
			}
		}
		return null;
	}

	/**
	 * Returns medication implied by medOrder by its code, or null if not found.  Must be RxNorm; uses first found.
	 * @param medOrder
	 * @return
	 */
	private static Medication getMedImpliedByMedOrderByCode(MedicationOrder medOrder)
	{
		if (medOrder != null)
		{
			try
			{
				CodeableConcept medCC = medOrder.getMedicationCodeableConcept();
				List<Coding> medCodingList = medCC.getCoding();
				for (Coding medCoding : medCodingList)
				{
					String medCodingSystem = medCoding.getSystem();
					if ((medCodingSystem != null) && (medCodingSystem.equals("http://www.nlm.nih.gov/research/umls/rxnorm")))
					{
						String medCode = medCoding.getCode();
						if (medCode != null)
						{
							Medication impliedMed = new Medication();
							impliedMed.setCode(medCC);
							return impliedMed;
						}
					}
				}
			}
			catch(Exception e)
			{
				// do nothing; may be an expected outcome.
			}
		}
		return null;
	}

	/**
	 * Looks in meds for the Practitioner prescriber referenced by medOrder.  Returns first one found, or null if not found.
	 * @param medOrder
	 * @param practitioners
	 * @return
	 */
	private static Practitioner getPrescriberReferencedByMedOrder(MedicationOrder medOrder, List<Practitioner> practitioners)
	{
		if ((medOrder != null) && (practitioners != null))
		{
			try
			{
				String prescriberIdFromOrder = ((Reference) medOrder.getPrescriber()).getReference();

				for (Practitioner practitioner : practitioners)
				{
					String practitionerId = practitioner.getId();

					if ((practitionerId.equals(prescriberIdFromOrder)) || ((practitionerId.contains("Practitioner/")) && (prescriberIdFromOrder.contains(practitionerId)))) // account for reference in medication order that uses absolute path to rest service
					{
						return practitioner;
					}
				}
			}
			catch (Exception e)
			{
				// do nothing; may be an expected outcome.
			}
		}

		return null;
	}

	/**
	 * Returns prescriber contained in medOrder, or null if not found.
	 * @param medOrder
	 * @return
	 */
	private static Practitioner getPrescriberContainedByMedOrder(MedicationOrder medOrder)
	{
		if (medOrder != null)
		{
			try
			{
				String prescriberIdFromOrder = ((Reference) medOrder.getPrescriber()).getReference();
				if ((prescriberIdFromOrder != null) && (prescriberIdFromOrder.startsWith("#")))
				{
					List<Resource> contained = medOrder.getContained();
					for (Resource containedResource : contained)
					{
						if (containedResource instanceof Practitioner)
						{
							String containedResourceId = containedResource.getId();
							if ((containedResourceId != null) && (containedResourceId.equals(prescriberIdFromOrder)))
							{
								return (Practitioner) containedResource;
							}
						}
					}
				}
			}
			catch(Exception e)
			{
				// do nothing; may be an expected outcome.
			}
		}
		return null;
	}

	/**
	 * Returns prescriber implied by medOrder by its reference displayname, or null if not found.
	 * @param medOrder
	 * @return
	 */
	private static Practitioner getPrescriberImpliedByMedOrderReferenceDisplay(MedicationOrder medOrder)
	{
		if (medOrder != null)
		{
			try
			{
				String prescriberDisplayFromOrder = ((Reference) medOrder.getPrescriber()).getDisplay();
				if(prescriberDisplayFromOrder != null)
				{
					Practitioner prescriber = new Practitioner();
					HumanName name = new HumanName();
					name.setText(prescriberDisplayFromOrder);
					prescriber.setName(name);
					return prescriber;
				}
			}
			catch(Exception e)
			{
				// do nothing; may be an expected outcome.
			}
		}
		return null;
	}

	/**
	 * Looks in meds for the Medication referenced by medOrder.  Returns first one found, or null if not found.
	 * @param medOrder
	 * @param meds
	 * @return
	 */
	public static Medication getMedReferencedByMedOrder(MedicationOrder medOrder, List<Medication> meds)
	{
		if ((medOrder != null) && (meds != null))
		{
			try
			{
				String medIdFromOrder = ((Reference) medOrder.getMedication()).getReference();

				for (Medication med : meds)
				{
					String medId = med.getId();

					//System.out.println("MED ID FROM ORDER: " + medIdFromOrder);
					//System.out.println("MED ID: " + medId);

					if ((medId.equals(medIdFromOrder)) || ((medId.contains("Medication/")) && (medIdFromOrder.contains(medId)))) // account for reference in medication order that uses absolute path to rest service
					{
						return med;
					}
				}
			}
			catch (Exception e)
			{
				// do nothing; may be an expected outcome.
			}
		}

		return null;
	}

	/**
	 * Over-writes existing dosage instruction info if the the medication is NOT missing the dose quantity.
	 *
	 * Checks that existing data is sufficient (Checks there there is at least an existing dosageInstruction with the sig.)
	 * If specified, proceeds regardless of whether parsed sig is stated as being able to be used without caution.
	 * @param medOrder
	 * @param proceedEvenIfRxSigStatesUseWithCaution
	 */
	public static void updateMedOrderDosageInstructionWithSigInfo(MedicationOrder medOrder, boolean proceedEvenIfRxSigStatesUseWithCaution)
	{
		if (medOrder == null || !isMedicationOrderMissingDoseQuantity(medOrder)) {
		    return;
		} else if (medOrder.getDosageInstruction().size() == 0) {
		    log.warn("medOrder " + medOrder.getId() + " does not have dosageInstructions.");
		    return;
		}
		// TODO: if needed, consider more than 1 dosage instruction (currently, will just takes and replaces first)
		MedicationOrderDosageInstructionComponent dosageInstruction = medOrder.getDosageInstruction().get(0);
		String dosageInstructionText = dosageInstruction.getText();
		RxSig sig = new RxSig(dosageInstructionText);

		// proceed only if caution flag is false, or if client requests proceeding regardless of caution flag
		if (proceedEvenIfRxSigStatesUseWithCaution || sig.useWithCaution() == false)
		{
			// route -- proceed if no existing route, and provide text
			if (dosageInstruction.getRoute() == null && sig.getRoute() != null)
			{
				CodeableConcept routeCodeableConcept = new CodeableConcept();
				routeCodeableConcept.setText(sig.getRoute());
				dosageInstruction.setRoute(routeCodeableConcept);
			}

			// prn -- proceed if no existing prn specified
			if (dosageInstruction.getAsNeeded() == null)
			{
				BooleanType prn = new BooleanType(sig.isPrn());
				dosageInstruction.setAsNeeded(prn);
			}

			// method -- proceed if no existing method specified, and provide text
			if (dosageInstruction.getMethod() == null && sig.getCommand() != null)
			{
				CodeableConcept methodCodeableConcept = new CodeableConcept();
				methodCodeableConcept.setText(sig.getCommand());
				dosageInstruction.setMethod(methodCodeableConcept);
			}

			// dose
			if (sig.getDoseLow() != null)
			{
				SimpleQuantity simpleQuantity_low = new SimpleQuantity();
				simpleQuantity_low.setValue(new BigDecimal(sig.getDoseLow()));
				if (sig.getDoseUnits() != null)
				{
					simpleQuantity_low.setUnit(sig.getDoseUnits());
				}

				if (! sig.isDoseRanged())
				{
					dosageInstruction.setDose(simpleQuantity_low);
				}
				else
				{
					SimpleQuantity simpleQuantity_high = new SimpleQuantity();
					simpleQuantity_high.setValue(new BigDecimal(sig.getDoseHigh()));
					if (sig.getDoseUnits() != null)
					{
						simpleQuantity_high.setUnit(sig.getDoseUnits());
					}

					Range doseRange = new Range();
					doseRange.setLow(simpleQuantity_low);
					doseRange.setHigh(simpleQuantity_high);
					dosageInstruction.setDose(doseRange);
				}
			}

			// timing
			if (((sig.getFrequencyLow() != null) && (sig.getFrequencyTimeUnits() != null)) || ((sig.getIntervalLow() != null) && (sig.getIntervalTimeUnits() != null)))
			{
				Timing timing = new Timing();
				TimingRepeatComponent repeat = new TimingRepeatComponent();

				try
				{
					Period originalPeriod = dosageInstruction.getTiming().getRepeat().getBoundsPeriod();
					repeat.setBounds(originalPeriod);
				}
				catch (Exception e)
				{
					// ignore
				}

				if ((sig.getFrequencyLow() != null) && (sig.getFrequencyTimeUnits() != null))
				{
					repeat.setFrequency(sig.getFrequencyLow());
					repeat.setPeriod(new BigDecimal(1));
					repeat.setPeriodUnits(getUnitsOfTimeFromStr(sig.getFrequencyTimeUnits()));

					if ((sig.getFrequencyHigh() != null) && (sig.getFrequencyHigh() != sig.getFrequencyLow()))
					{
						repeat.setFrequencyMax(sig.getFrequencyHigh());
					}
				}
				else // if ((sig.getIntervalLow() != null) && (sig.getIntervalTimeUnits() != null))
				{
					repeat.setPeriod(new BigDecimal(sig.getIntervalLow()));
					repeat.setPeriodUnits(getUnitsOfTimeFromStr(sig.getIntervalTimeUnits()));

					if ((sig.getIntervalHigh() != null) && (sig.getIntervalHigh() != sig.getIntervalLow()))
					{
						repeat.setPeriodMax(new BigDecimal(sig.getIntervalHigh()));
					}
				}

				timing.setRepeat(repeat);
				dosageInstruction.setTiming(timing);
			}
		}
	}

	public static UnitsOfTime getUnitsOfTimeFromStr(String timeUnitStr)
	{
		if (timeUnitStr != null)
		{
			if (timeUnitStr.equalsIgnoreCase("a") || timeUnitStr.equalsIgnoreCase("yr") || timeUnitStr.equalsIgnoreCase("year"))
			{
				return UnitsOfTime.A;
			}
			else if (timeUnitStr.equalsIgnoreCase("d") || timeUnitStr.equalsIgnoreCase("day"))
			{
				return UnitsOfTime.D;
			}
			else if (timeUnitStr.equalsIgnoreCase("h") || timeUnitStr.equalsIgnoreCase("hr") || timeUnitStr.equalsIgnoreCase("hour"))
			{
				return UnitsOfTime.H;
			}
			else if (timeUnitStr.equalsIgnoreCase("min") || timeUnitStr.equalsIgnoreCase("minute"))
			{
				return UnitsOfTime.MIN;
			}
			else if (timeUnitStr.equalsIgnoreCase("mo") || timeUnitStr.equalsIgnoreCase("month"))
			{
				return UnitsOfTime.MO;
			}
			else if (timeUnitStr.equalsIgnoreCase("s") || timeUnitStr.equalsIgnoreCase("sec") || timeUnitStr.equalsIgnoreCase("second"))
			{
				return UnitsOfTime.S;
			}
			else if (timeUnitStr.equalsIgnoreCase("wk") || timeUnitStr.equalsIgnoreCase("week"))
			{
				return UnitsOfTime.WK;
			}
		}
		return null;
	}

	/**
	 * Returns encounters in list where period.start falls within the specified time intervals.
	 * If specific types or statuses required, only counts those meeting those requirements.
	 * @param encounters
	 * @param earliestStartDateInclusive
	 * @param latestStartDateExclusive
	 * @param filterByEncounterStatus
	 * @param allowedEncounterStatuses_mayBeNull
	 * @param filterByEncounterType
	 * @param allowedEncounterTypes_mayBeNull
	 * @return
	 */
	public static List<Encounter> getEncountersMatchingCriteria(List<Encounter> encounters, Date earliestStartDateInclusive, Date latestStartDateExclusive, boolean filterByEncounterStatus, List<EncounterState> allowedEncounterStatuses_mayBeNull, boolean filterByEncounterType, List<CodeableConcept> allowedEncounterTypes_mayBeNull)
	{
		List<Encounter> encountersToReturn = new ArrayList<Encounter>();

		if ((encounters != null) && (earliestStartDateInclusive != null) && (latestStartDateExclusive != null))
		{
			for (Encounter encounter : encounters)
			{
				Period period = encounter.getPeriod();
				if (period != null)
				{
					Date start = period.getStart();
					if (start != null)
					{
						if ((! start.before(earliestStartDateInclusive)) && (start.before(latestStartDateExclusive)))
						{
							boolean meetsStatusReq = false;
							boolean meetsTypeReq = false;

							if (filterByEncounterStatus == false)
							{
								meetsStatusReq = true;
							}
							else
							{
								EncounterState encounterState = encounter.getStatus();
								if ((encounterState != null) && (allowedEncounterStatuses_mayBeNull != null))
								{
									if (allowedEncounterStatuses_mayBeNull.contains(encounterState))
									{
										meetsStatusReq = true;
									}
								}
							}

							if (filterByEncounterType == false)
							{
								meetsTypeReq = true;
							}
							else
							{
								List<CodeableConcept> types = encounter.getType();
								if ((types != null) && (allowedEncounterTypes_mayBeNull != null))
								{
									for (CodeableConcept type : types)
									{
										if(isCodingContains(type, allowedEncounterTypes_mayBeNull))
										{
											meetsStatusReq = true;
										}
									}
								}
							}

							if (meetsStatusReq && meetsTypeReq)
							{
								encountersToReturn.add(encounter);
							}
						}
					}
				}
			}
		}
		return encountersToReturn;
	}

	/**
	 * Returns all dates between (anchorDate - daysToLookBack) and anchorDate (inclusive) covered by one of the medication orders.
	 * Returns in ascending order.
	 * Uses medOrder.dosageInstructions.dosageInstruction.timing.repeat.boundsPeriod.start and end.  If either is null, secondarily uses
	 * medOrderIdToNameValuePairMap_optional contents for "startDate" (Date) and "endDate" (Date), or "endDatePreferDefault" (Date) if so specified.
	 * Start Date must be present.
	 * If End Date still null after all the other processing, assumed to continue indefinitely.
	 * @param anchorDate
	 * @param daysToLookBack
	 * @param medOrders
	 * @param medOrderIdToNameValuePairMap_optional
	 * @param useEndDatePreferDefault
	 * @return
	 */
	public static List<Date> getDaysCoveredByMedOrders(Date anchorDate, int daysToLookBack, List<MedicationOrder> medOrders, Map<String,Map<String, Object>> medOrderIdToNameValuePairMap_optional, boolean useEndDatePreferDefault)
	{
		HashSet<Date> setToReturn = new HashSet<Date>();

		DateUtility dateUtility = DateUtility.getInstance();

		if ((medOrders != null) && (anchorDate != null) && (daysToLookBack >= 0))
		{
			System.out.println();
			ArrayList<Date> potentialDates = new ArrayList<Date>();
			Date anchorDateWithoutHrsMinSec = dateUtility.getDateFromString(dateUtility.getDateAsString(anchorDate, "yyyy-MM-dd"), "yyyy-MM-dd");
			potentialDates.add(anchorDate);

			for (int k = 0; k < daysToLookBack; k++)
			{
				Date potentialDate = dateUtility.getDateAfterAddingTime(anchorDateWithoutHrsMinSec, Calendar.DATE, -1 * (k + 1));
				potentialDates.add(potentialDate);
			}

			for (MedicationOrder medOrder : medOrders)
			{
				String medOrderId = medOrder.getId();

				List<MedicationOrderDosageInstructionComponent> dosageInstructionList = medOrder.getDosageInstruction();
				if (dosageInstructionList != null)
				{
					for(MedicationOrderDosageInstructionComponent dosageInstruction : dosageInstructionList)
					{
						Timing timing = dosageInstruction.getTiming();
						if (timing != null)
						{
							TimingRepeatComponent repeat = timing.getRepeat();
							if (repeat != null)
							{
								Period period;
								try
								{
									period = repeat.getBoundsPeriod();
									// TODO: consider support other types of bounds
									if (period != null)
									{
										Date start = period.getStart();
										Date end = period.getEnd();

										if (start == null)
										{
											try
											{
												start = (Date) medOrderIdToNameValuePairMap_optional.get(medOrderId).get("startDate");
											}
											catch (Exception e)
											{
												// do nothing
											}
										}

										if (end== null)
										{
											try
											{
												if (useEndDatePreferDefault)
												{
													end = (Date) medOrderIdToNameValuePairMap_optional.get(medOrderId).get("endDatePreferDefault");
												}
												else
												{
													end = (Date) medOrderIdToNameValuePairMap_optional.get(medOrderId).get("endDate");
												}
											}
											catch (Exception e)
											{
												// do nothing
											}
										}

										if (start != null)
										{
											Date startDateWithoutHrsMinSec = dateUtility.getDateFromString(dateUtility.getDateAsString(start, "yyyy-MM-dd"), "yyyy-MM-dd");

											Date endDateWithoutHrsMinSec = null;
											if (end != null)
											{
												endDateWithoutHrsMinSec = dateUtility.getDateFromString(dateUtility.getDateAsString(end, "yyyy-MM-dd"), "yyyy-MM-dd");
											}

											for (Date potentialDate : potentialDates)
											{
												if (end == null)
												{
													if (! potentialDate.before(startDateWithoutHrsMinSec))
													{
														setToReturn.add(potentialDate);
													}
												}
												else
												{
													if ((! potentialDate.before(startDateWithoutHrsMinSec)) && (! potentialDate.after(endDateWithoutHrsMinSec)))
													{
														setToReturn.add(potentialDate);
													}
												}
											}
										}
									}
								}
								catch (Exception e)
								{
									// do nothing
								}
							}
						}
					}
				}
			}
		}

		ArrayList<Date> listToReturn = new ArrayList<Date>(setToReturn);
		Collections.sort(listToReturn);
		return listToReturn;
	}

	public static boolean systemOutPrintln(String textToDisplay)
	{
		java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");
		Date date = new Date();
		System.out.println(dateFormat.format(date) + " " + textToDisplay );
		return true;
	}

	/**
	 * Adds to map the setEntry for a Set defined by the key.  If set does not exist, creates one.  If setEntry already entered, does not add.
	 * Returns true if new entry was added, false if the entry already existed.
	 * @param map
	 * @param key
	 * @param setEntry
	 * @return
	 */
	public static boolean addToMapSet(HashMap map, String key, String setEntry)
	{
		if (map != null)
		{
			Set set = (Set) map.get(key);
			if (set == null)
			{
				set = new HashSet<String>();
				set.add(setEntry);
				map.put(key, set);
				return true;
			}
			else
			{
				if (set.contains(setEntry))
				{
					return false;
				}
				else
				{
					set.add(setEntry);
					map.put(key, set);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Adds to map the setEntry for a Set defined by the key.  If set does not exist, creates one.  If setEntry already entered, does not add.
	 * Returns true if new entry was added, false if the entry already existed.
	 * @param map
	 * @param key
	 * @param setEntry
	 * @return
	 */
	public static boolean addToMapSet(HashMap map, Integer key, String setEntry)
	{
		if (map != null)
		{
			Set set = (Set) map.get(key);
			if (set == null)
			{
				set = new HashSet<String>();
				set.add(setEntry);
				map.put(key, set);
				return true;
			}
			else
			{
				if (set.contains(setEntry))
				{
					return false;
				}
				else
				{
					set.add(setEntry);
					map.put(key, set);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Adds to map the setEntry for a Set defined by the key.  If set does not exist, creates one.  If setEntry already entered, does not add.
	 * Returns true if new entry was added, false if the entry already existed.
	 * @param map
	 * @param key
	 * @param setEntry
	 * @return
	 */
	public static boolean addToMapSet(HashMap map, Integer key, Integer setEntry)
	{
		if (map != null)
		{
			Set set = (Set) map.get(key);
			if (set == null)
			{
				set = new HashSet<Integer>();
				set.add(setEntry);
				map.put(key, set);
				return true;
			}
			else
			{
				if (set.contains(setEntry))
				{
					return false;
				}
				else
				{
					set.add(setEntry);
					map.put(key, set);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Searches parameters for the following, and returns map with entries as follows (e.g., entry for "clientLanguage" whose value is a String):
	 * - clientLanguage (String): looks for a parameter with the name "Client Language", case-insensitive, and space-insensitive.  If found, returns that.  If not found and
	 *   an input global is already defined and is not "", returns that.  Else returns "en-US".
	 * 		Language shall be specifiied as either a 2-character ISO 639-1 language code or a combination of a 2-character ISO 639-1 language code
	 * 		and a 2-character ISO 3166-1 geographical code, concatenated with a hyphen. Example valid language specifications include: en, en-US,
	 * 		en-GB, and fr. ISO 639-1 codes are available at http://www.loc.gov/standards/iso639-2/php/English_list.php, and ISO 3166-1 codes are
	 * 		available at http://www.iso.org/iso/home/standards/country_codes/country_names_and_code_elements.htm. See Section 5 for normative references to these standards.
	 * - clientTimeZoneOffset (String): looks for a parameter with the name "Client Time Zone Offset", case-insensitive, and space-insensitive.  If found, returns that.  If not found and
	 *   an input global is already defined, returns that.  Else returns null.
	 * 		Time zone offset is from Universal Coordinated Time (UTC). This offset is expressed as +/- hh:mm, e.g., 00:00, -05:00, +07:00.
	 * 		Note that the client's time zone offset cannot be used to determine a geographical time zone. Unless otherwise specified, all time-stamped
	 * 		data provided by the client will be assumed to have this time zone offset.	 *
	 * - evalTime (DateTimeDt): looks for a parameter with the name "Eval Time", case-insensitive, and space-insensitive.  If found, returns that.  If not found and
	 *   an input global is already defined, returns that.  Else returns current time.
	 *   Eval Time shall be specified without fuzziness, at least down to the date.
	 * - focalPatientId (IdentifierDt): looks for a parameter with the name "Focal Patient Id", case-insensitive, and space-insensitive.  If found, returns that.  Else returns null.
	 *	 Should be at least a system and value.
	 * @param parameters20
	 * @param clientLanguage		global value
	 * @param clientTimeZoneOffset	global value
	 * @param evalTime				global value
	 */
	public static HashMap<String, Object> getGlobalsFromParameters(Parameters parameters20, String clientLanguage, String clientTimeZoneOffset, Date evalTime)
	{
		HashMap<String, Object> mapToReturn = new HashMap<String, Object>();

		boolean clientLanguageFound = false;
		boolean clientTimeZoneOffsetFound = false;
		boolean evalTimeFound = false;
		boolean focalPatientIdFound = false;

		if (parameters20 != null)
		{
			List<ParametersParameterComponent> parameterList = parameters20.getParameter();
			for (ParametersParameterComponent parameter : parameterList)
			{
				String paramName = parameter.getName();
				if (paramName != null)
				{
					String paramNameNormalized = paramName.replaceAll(" ", "");

					if (paramNameNormalized.equalsIgnoreCase("ClientLanguage"))
					{
						try
						{
							mapToReturn.put("clientLanguage", ((StringType) parameter.getValue()).getValue());
							clientLanguageFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a string for Parameter Client Language.");
						}
					}
					else if (paramNameNormalized.equalsIgnoreCase("ClientTimeZoneOffset"))
					{
						try
						{
							mapToReturn.put("clientTimeZoneOffset", ((StringType) parameter.getValue()).getValue());
							clientTimeZoneOffsetFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a string for Parameter Client Time Zone Offset.");
						}
					}
					else if (paramNameNormalized.equalsIgnoreCase("EvalTime"))
					{
						try
						{
							DateTimeType evalTimeDate = (DateTimeType) parameter.getValue();
							mapToReturn.put("evalTime", evalTimeDate);
							evalTimeFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a DateTime for Parameter Eval Time.");
						}
					}
					else if (paramNameNormalized.equalsIgnoreCase("FocalPatientId"))
					{
						try
						{
							mapToReturn.put("focalPatientId", (Identifier) parameter.getValue());
							focalPatientIdFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get an Identifier for Parameter Focal Patient ID.");
						}
					}
				}
				if (clientLanguageFound && clientTimeZoneOffsetFound && evalTimeFound && focalPatientIdFound)
				{
					break;
				}
			}
		}

		if (! clientLanguageFound)
		{
			if ((clientLanguage != null) && (!clientLanguage.equals("")))
			{
				mapToReturn.put("clientLanguage", clientLanguage);
			}
			else
			{
				mapToReturn.put("clientLanguage", "en-US");
			}
		}

		if (! clientTimeZoneOffsetFound)
		{
			if ((clientTimeZoneOffset != null) && (!clientTimeZoneOffset.equals("")))
			{
				mapToReturn.put("clientTimeZoneOffset", clientTimeZoneOffset);
			}
		}

		if (! evalTimeFound)
		{
			if (evalTime != null)
			{
				DateTimeType myOtherTime = new DateTimeType();
				myOtherTime.setValue(evalTime);
				mapToReturn.put("evalTime", myOtherTime);
			}
			else
			{
				Date date = new Date();
				DateTimeType myDateTime = new DateTimeType(date);
				mapToReturn.put("evalTime", myDateTime);
			}
		}

		return mapToReturn;
	}

	/**
	 * Searches parameters for the following, and returns map with entries as follows (e.g., entry for "clientLanguage" whose value is a String):
	 * - clientLanguage (String): looks for a parameter with the name "Client Language", case-insensitive, and space-insensitive.  If found, returns that.  If not found and
	 *   an input global is already defined and is not "", returns that.  Else returns "en-US".
	 * 		Language shall be specifiied as either a 2-character ISO 639-1 language code or a combination of a 2-character ISO 639-1 language code
	 * 		and a 2-character ISO 3166-1 geographical code, concatenated with a hyphen. Example valid language specifications include: �en,� �en-US,�
	 * 		�en-GB,� and �fr.� ISO 639-1 codes are available at http://www.loc.gov/standards/iso639-2/php/English_list.php, and ISO 3166-1 codes are
	 * 		available at http://www.iso.org/iso/home/standards/country_codes/country_names_and_code_elements.htm. See Section 5 for normative references to these standards.
	 * - clientTimeZoneOffset (String): looks for a parameter with the name "Client Time Zone Offset", case-insensitive, and space-insensitive.  If found, returns that.  If not found and
	 *   an input global is already defined, returns that.  Else returns null.
	 * 		Time zone offset is from Universal Coordinated Time (UTC). This offset is expressed as +/- hh:mm, e.g., 00:00, -05:00, +07:00.
	 * 		Note that the client's time zone offset cannot be used to determine a geographical time zone. Unless otherwise specified, all time-stamped
	 * 		data provided by the client will be assumed to have this time zone offset.	 *
	 * - evalTime (DateTimeDt): looks for a parameter with the name "Eval Time", case-insensitive, and space-insensitive.  If found, returns that.  If not found and
	 *   an input global is already defined, returns that.  Else returns current time.
	 *   Eval Time shall be specified without fuzziness, at least down to the date.
	 * - focalPatientId (IdentifierDt): looks for a parameter with the name "Focal Patient Id", case-insensitive, and space-insensitive.  If found, returns that.  Else returns null.
	 *	 Should be at least a system and value.
	 * @param parameters21
	 * @param clientLanguage		global value
	 * @param clientTimeZoneOffset	global value
	 * @param evalTime				global value
	 */
	public static HashMap<String, Object> getGlobalsFromParameters21(org.hl7.fhir.dstu3.model.Parameters parameters21, String clientLanguage, String clientTimeZoneOffset, Date evalTime)
	{
		HashMap<String, Object> mapToReturn = new HashMap<String, Object>();

		boolean clientLanguageFound = false;
		boolean clientTimeZoneOffsetFound = false;
		boolean evalTimeFound = false;
		boolean focalPatientIdFound = false;

		if (parameters21 != null)
		{
			List<org.hl7.fhir.dstu3.model.Parameters.ParametersParameterComponent> parameterList = parameters21.getParameter();
			for (org.hl7.fhir.dstu3.model.Parameters.ParametersParameterComponent parameter : parameterList)
			{
				String paramName = parameter.getName();
				if (paramName != null)
				{
					String paramNameNormalized = paramName.replaceAll(" ", "");

					if (paramNameNormalized.equalsIgnoreCase("ClientLanguage"))
					{
						try
						{
							mapToReturn.put("clientLanguage", ((org.hl7.fhir.dstu3.model.StringType) parameter.getValue()).getValue());
							clientLanguageFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a string for Parameter Client Language.");
						}
					}
					else if (paramNameNormalized.equalsIgnoreCase("ClientTimeZoneOffset"))
					{
						try
						{
							mapToReturn.put("clientTimeZoneOffset", ((org.hl7.fhir.dstu3.model.StringType) parameter.getValue()).getValue());
							clientTimeZoneOffsetFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a string for Parameter Client Time Zone Offset.");
						}
					}
					else if ((paramNameNormalized.equalsIgnoreCase("EvalTime")) || (paramNameNormalized.equalsIgnoreCase("evaluateAtDateTime")))
					{
						try
						{
							org.hl7.fhir.dstu3.model.DateTimeType evalTimeDate = (org.hl7.fhir.dstu3.model.DateTimeType) parameter.getValue();
							mapToReturn.put("evalTime", evalTimeDate);
							evalTimeFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a DateTime for Parameter Eval Time.");
						}
					}
					else if (paramNameNormalized.equalsIgnoreCase("FocalPatientId"))
					{
						try
						{
							mapToReturn.put("focalPatientId", (org.hl7.fhir.dstu3.model.Identifier) parameter.getValue()); // this mapping may not be right
							focalPatientIdFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get an Identifier for Parameter Focal Patient ID.");
						}
					}
				}
				if (clientLanguageFound && clientTimeZoneOffsetFound && evalTimeFound && focalPatientIdFound)
				{
					break;
				}
			}
		}

		if (! clientLanguageFound)
		{
			if ((clientLanguage != null) && (!clientLanguage.equals("")))
			{
				mapToReturn.put("clientLanguage", clientLanguage);
			}
			else
			{
				mapToReturn.put("clientLanguage", "en-US");
			}
		}

		if (! clientTimeZoneOffsetFound)
		{
			if ((clientTimeZoneOffset != null) && (!clientTimeZoneOffset.equals("")))
			{
				mapToReturn.put("clientTimeZoneOffset", clientTimeZoneOffset);
			}
		}

		if (! evalTimeFound)
		{
			if (evalTime != null)
			{
				DateTimeType myOtherTime = new DateTimeType();
				myOtherTime.setValue(evalTime);
				mapToReturn.put("evalTime", myOtherTime);
			}
			else
			{
				Date date = new Date();
				DateTimeType myDateTime = new DateTimeType();
				myDateTime.setValue(date);
				mapToReturn.put("evalTime", myDateTime);
			}
		}

		return mapToReturn;
	}

	/**
	 * Expects reason to be sent as a CodeableConcept.  Allows it to be passed from the parent Order.
	 * Returns output Parameters as GAO Parameters.  If being used in CQIF context, expected to be return with the response object as nested parameters.
	 * @param parameters21
	 * @return
	 */

//	public static org.hl7.fhir.dstu3.model.Parameters getGaoEvaluationResult (org.hl7.fhir.instance.model.api.dstu3.DiagnosticOrder diagnosticOrder, org.hl7.fhir.dstu3.model.Order order, org.hl7.fhir.dstu3.model.CodeableConcept reason)
//	{
//		/*
//		 * TODO: this is just an example.
//		 * Example here: diabetes lab testing
//		 *
//		 * WE ARE JUST IMPLEMENTING CASE WHERE HgbA1c is being used for Diabetes diagnosis
//		 *
//		 * Test					Rationale					Appropriateness
//		 * HgbA1c				Diabetes diagnosis			3
//		 * Fasting glucose		Diabetes diagnosis			5
//		 * Random glucose		Diabetes diagnosis			2
//		 * GTT					Diabetes diagnosis			10
//		 *
//		 * HgbA1c				Diabetes monitoring			10
//		 * Fasting glucose		Diabetes monitoring			8
//		 * Random glucose		Diabetes monitoring			6
//		 * GTT					Diabetes monitoring			2
//		 *
//		 * Codes expected:		codeSystem			code
//		 * HgbA1c				http://loinc.org 	4548-4			(total in blood)
//		 * Fasting glucose		http://loinc.org	1493-6			(in serum or plasma)
//		 * Random glucose		http://loinc.org	2352-3
//		 * GTT					http://loinc.org	6751-2			(2hr)
//		 *
//		 * Diabetes diagnosis		opencds			"DiabetesDiagnosis"
//		 * Diabetes monitoring		opencds			"DiabetesMonitoring"
//		 */
//		org.hl7.fhir.dstu3.model.Parameters paramsToReturn = new org.hl7.fhir.dstu3.model.Parameters();
//		if (diagnosticOrder != null)
//		{
//			// assume that the diagnostic order contains a single item with a single code
//			List<DiagnosticOrderItemComponent> itemComponents = diagnosticOrder.getItem();
//			if(itemComponents != null)
//			{
//				DiagnosticOrderItemComponent itemComponent = itemComponents.get(0);
//				if (itemComponent != null)
//				{
//					org.hl7.fhir.dstu3.model.CodeableConcept cc = itemComponent.getCode();
//					if (Utils.isCodingContains3(cc, "http://loinc.org", "4548-4"))
//					{
//						if (reason != null)
//						{
//							if (Utils.isCodingContains3(reason, "opencds", "DiabetesDiagnosis"))
//							{
//								// order
//								ParametersParameterComponent orderParam = new ParametersParameterComponent(new StringType("order"));
//								orderParam.setResource(order);
//								paramsToReturn.addParameter(orderParam);
//
//
//								// result
//								ParametersParameterComponent resultParam = new ParametersParameterComponent(new StringType("result"));
//								org.hl7.fhir.dstu3.model.Basic result = new org.hl7.fhir.dstu3.model.Basic();
//								result.setId("1");
//								org.hl7.fhir.dstu3.model.Meta resultMeta = new org.hl7.fhir.dstu3.model.Meta();
//								resultMeta.addProfile("http://hl7.org/fhir/StructureDefinition/gao-result");
//								result.setMeta(resultMeta);
//									// result score
//								org.hl7.fhir.dstu3.model.Extension resultExtensionScore = new org.hl7.fhir.dstu3.model.Extension();
//								resultExtensionScore.setUrl("http://hl7.org/fhir/StructureDefinition/gao-extension-score");
//								resultExtensionScore.setValue(new org.hl7.fhir.dstu3.model.DecimalType(3));
//								result.addExtension(resultExtensionScore);
//
//									// result item
//								org.hl7.fhir.dstu3.model.Extension resultExtensionItem = new org.hl7.fhir.dstu3.model.Extension();
//								resultExtensionItem.setUrl("http://hl7.org/fhir/StructureDefinition/gao-extension-item");
//
//								//org.hl7.fhir.dstu21.model.Meta resultItemMeta = new org.hl7.fhir.dstu21.model.Meta();
//								//resultItemMeta.addProfile("http://hl7.org/fhir/StructureDefinition/gao-extension-item");
//
//
//								org.hl7.fhir.dstu3.model.Extension resultExtensionItemCode = new org.hl7.fhir.dstu3.model.Extension();
//								resultExtensionItemCode.setUrl("http://hl7.org/fhir/StructureDefinition/gao-extension-item-code"); // TODO: this is a bogus extension URL, needed to not throw an error in HAPI-FHIR
//								org.hl7.fhir.dstu3.model.CodeableConcept itemCodeCC = new org.hl7.fhir.dstu3.model.CodeableConcept();
//								org.hl7.fhir.dstu3.model.Coding itemCodeCoding = new org.hl7.fhir.dstu3.model.Coding();
//								itemCodeCoding.setSystem("http://loinc.org");
//								itemCodeCoding.setCode("4548-4");
//								itemCodeCC.addCoding(itemCodeCoding);
//								resultExtensionItemCode.setValue(itemCodeCC);
//								resultExtensionItem.addExtension(resultExtensionItemCode);
//								result.addExtension(resultExtensionItem);
//
//									// result code
//								org.hl7.fhir.dstu3.model.CodeableConcept resultCodeCC = new org.hl7.fhir.dstu3.model.CodeableConcept();
//								org.hl7.fhir.dstu3.model.Coding resultCodeCoding = new org.hl7.fhir.dstu3.model.Coding();
//								resultCodeCoding.setSystem("http://hl7.org/fhir/evaluation-result-code");
//								resultCodeCoding.setCode("outside");
//								resultCodeCC.addCoding(resultCodeCoding);
//								result.setCode(resultCodeCC);
//
//								resultParam.setResource(result);
//								paramsToReturn.addParameter(resultParam);
//
//								// dss
//								ParametersParameterComponent dssParam = new ParametersParameterComponent(new StringType("dss"));
//								org.hl7.fhir.dstu3.model.Device device = new org.hl7.fhir.dstu3.model.Device();
//								org.hl7.fhir.dstu3.model.CodeableConcept deviceTypeCC = new org.hl7.fhir.dstu3.model.CodeableConcept();
//								org.hl7.fhir.dstu3.model.Coding deviceTypeCoding = new org.hl7.fhir.dstu3.model.Coding();
//								deviceTypeCoding.setSystem("opencds");
//								deviceTypeCoding.setCode("OpenCDS");
//								deviceTypeCC.addCoding(deviceTypeCoding);
//								device.setType(deviceTypeCC);
//								dssParam.setResource(device);
//								paramsToReturn.addParameter(dssParam);
//							}
//						}
//					}
//				}
//			}
//		}
//		return paramsToReturn;
	/*

		if (parameters21 != null)
		{
			List<org.hl7.fhir.dstu21.model.Parameters.ParametersParameterComponent> parameterList = parameters21.getParameter();
			for (org.hl7.fhir.dstu21.model.Parameters.ParametersParameterComponent parameter : parameterList)
			{
				String paramName = parameter.getName();
				if (paramName != null)
				{
					String paramNameNormalized = paramName.replaceAll(" ", "");

					if (paramNameNormalized.equalsIgnoreCase("ClientLanguage"))
					{
						try
						{
							mapToReturn.put("clientLanguage", ((org.hl7.fhir.dstu21.model.StringType) parameter.getValue()).getValue());
							clientLanguageFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a string for Parameter Client Language.");
						}
					}
					else if (paramNameNormalized.equalsIgnoreCase("ClientTimeZoneOffset"))
					{
						try
						{
							mapToReturn.put("clientTimeZoneOffset", ((org.hl7.fhir.dstu21.model.StringType) parameter.getValue()).getValue());
							clientTimeZoneOffsetFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a string for Parameter Client Time Zone Offset.");
						}
					}
					else if (paramNameNormalized.equalsIgnoreCase("EvalTime"))
					{
						try
						{
							org.hl7.fhir.dstu21.model.DateTimeType evalTimeDate = (DateTimeType) parameter.getValue();
							mapToReturn.put("evalTime", evalTimeDate);
							evalTimeFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get a DateTime for Parameter Eval Time.");
						}
					}
					else if (paramNameNormalized.equalsIgnoreCase("FocalPatientId"))
					{
						try
						{
							mapToReturn.put("focalPatientId", (org.hl7.fhir.dstu21.model.Identifier) parameter.getValue()); // this mapping may not be right
							focalPatientIdFound = true;
						}
						catch(Exception e)
						{
							System.err.println("Error in Utils.getGlobalsFromParameters: could not get an Identifier for Parameter Focal Patient ID.");
						}
					}
				}
				if (clientLanguageFound && clientTimeZoneOffsetFound && evalTimeFound && focalPatientIdFound)
				{
					break;
				}
			}
		}

		if (! clientLanguageFound)
		{
			if ((clientLanguage != null) && (!clientLanguage.equals("")))
			{
				mapToReturn.put("clientLanguage", clientLanguage);
			}
			else
			{
				mapToReturn.put("clientLanguage", "en-US");
			}
		}

		if (! clientTimeZoneOffsetFound)
		{
			if ((clientTimeZoneOffset != null) && (!clientTimeZoneOffset.equals("")))
			{
				mapToReturn.put("clientTimeZoneOffset", clientTimeZoneOffset);
			}
		}

		if (! evalTimeFound)
		{
			if (evalTime != null)
			{
				DateTimeDt myOtherTime = new DateTimeDt();
				myOtherTime.setValue(evalTime);
				mapToReturn.put("evalTime", myOtherTime);
			}
			else
			{
				Date date = new Date();
				DateTimeDt myDateTime = new DateTimeDt(date);
				mapToReturn.put("evalTime", myDateTime);
			}
		}

		return mapToReturn;
		*/
//	}

	/**
	 * Returns true if the DateTime or Period occurred prior to evalTime.
	 * Returns false if it did not.
	 * Returns true or false if the answer is "maybe", depending on the countMayeAsTrue variable.
	 *
	 * Pre-condition: assumes evalTime is a "real" date-time, without need to consider ambiguities in the time it represents (e.g., "sometime in 2015").
	 *
	 * @param dateTimeDtOrPeriodDt
	 * @param evalTime
	 * @param countMaybeAsTrue
	 * @return
	 */
	public static boolean isDateTimeOrPeriodBefore(Type dateTimeDtOrPeriodDt, DateTimeType evalTime, boolean countMaybeAsTrue)
	{
		if ((dateTimeDtOrPeriodDt != null) && (evalTime != null))
		{
			boolean isDateTime = false;
			boolean isPeriod = false;
			DateTimeType dateTimeDt;
			Period periodDt;
			try
			{
				dateTimeDt = (DateTimeType) dateTimeDtOrPeriodDt;
				isDateTime = true;
			}
			catch (Exception e)
			{
			}
			if (! isDateTime)
			{
				try
				{
					periodDt = (Period) dateTimeDtOrPeriodDt;
					isPeriod = true;
				}
				catch (Exception e)
				{
				}
			}
			if (isDateTime)
			{
				// TODO: actually implement.  For now, just return true.
				// define the upper and lower range of time this represents
				return true;
			}
			else if (isPeriod)
			{
				// TODO: actually implement.  For now, just return true.
				// define the upper and lower range of time this represents
				return true;
			}
		}

		return false;
	}



	/**
	 * Attempt to convert free text frequency to a TimingDt object.  Attempts to populate the "repeat" element with available parameters.  If unable, returns empty object.
	 *
	 * TODO: this class is currently optimized just for patterns found in office visit ordering of opioids at University of Utah Health.  It should NOT be considered comprehensive/complete.
	 *
	 * @param freeTextFrequency
	 * @return
	 */
	public static Timing getTimingDtFromFreeTextFrequency(String freeTextFrequency, Date startDate, Date endDate)
	{
		Timing timingDtToReturn = new Timing();
		TimingRepeatComponent repeat = new TimingRepeatComponent();

		if (freeTextFrequency != null)
		{
			String textUpper = freeTextFrequency.toUpperCase();

			if (textUpper.startsWith("TWICE WEEKLY"))
			{
				repeat.setFrequency(2);
				repeat.setPeriod(BigDecimal.valueOf(1));
				repeat.setPeriodUnits(UnitsOfTime.WK);
			}
			else if (textUpper.startsWith("WEEKLY"))
			{
				repeat.setFrequency(1);
                repeat.setPeriod(BigDecimal.valueOf(1));
				repeat.setPeriodUnits(UnitsOfTime.WK);
			}
			else if (textUpper.startsWith("AT BEDTIME"))
			{
				repeat.setFrequency(1);
                repeat.setPeriod(BigDecimal.valueOf(1));
				repeat.setPeriodUnits(UnitsOfTime.D);
			}
			else if (textUpper.startsWith("DAILY"))
			{
				repeat.setFrequency(1);
                repeat.setPeriod(BigDecimal.valueOf(1));
				repeat.setPeriodUnits(UnitsOfTime.D);
			}
			else if (textUpper.startsWith("EVERY EVENING") || textUpper.startsWith("EVERY MORNING") || textUpper.startsWith("EVERY NIGHT"))
			{
				repeat.setFrequency(1);
                repeat.setPeriod(BigDecimal.valueOf(1));
				repeat.setPeriodUnits(UnitsOfTime.D);
			}
			else if (textUpper.startsWith("EVERY MONDDAY") || textUpper.startsWith("EVERY TUESDAY") || textUpper.startsWith("EVERY WEDNESDAY") || textUpper.startsWith("EVERY THURSDAY") || textUpper.startsWith("EVERY FRIDAY") || textUpper.startsWith("EVERY SATURDAY") || textUpper.startsWith("EVERY SUNDAY"))
			{
				repeat.setFrequency(1);
                repeat.setPeriod(BigDecimal.valueOf(1));
				repeat.setPeriodUnits(UnitsOfTime.WK);
			}
			else if (textUpper.startsWith("EVERY HOUR"))
			{
				repeat.setFrequency(1);
                repeat.setPeriod(BigDecimal.valueOf(1));
				repeat.setPeriodUnits(UnitsOfTime.H);
			}
			else if (textUpper.startsWith("EVERY OTHER DAY"))
			{
				repeat.setFrequency(1);
                repeat.setPeriod(BigDecimal.valueOf(2));
				repeat.setPeriodUnits(UnitsOfTime.D);
			}
			else if (textUpper.startsWith("EVERY"))
			{
				int frequency = getNumberFromString(textUpper);

				if (frequency >= 1)
				{
					repeat.setFrequency(frequency);
	                repeat.setPeriod(BigDecimal.valueOf(1));

					if (textUpper.contains("MIN") || textUpper.contains("MINUTES"))
					{
						repeat.setPeriodUnits(UnitsOfTime.MIN);
					}
					else if (textUpper.contains("HOURS"))
					{
						repeat.setPeriodUnits(UnitsOfTime.H);
					}
					else if (textUpper.contains("DAYS"))
					{
						repeat.setPeriodUnits(UnitsOfTime.D);
					}
					else if (textUpper.contains("WEEKS"))
					{
						repeat.setPeriodUnits(UnitsOfTime.WK);
					}
				}
			}
			else if (textUpper.contains("TIMES DAILY") || textUpper.contains("TIMES A DAY") || textUpper.contains("TIMES WEEKLY") || textUpper.contains("TIMES A WEEK"))
			{
				int frequency = getNumberFromString(textUpper);

				if (frequency >= 1)
				{
					repeat.setFrequency(frequency);
                    repeat.setPeriod(BigDecimal.valueOf(1));

					if (textUpper.contains("TIMES DAILY") || textUpper.contains("TIMES A DAY"))
					{
						repeat.setPeriodUnits(UnitsOfTime.D);
					}
					else if (textUpper.contains("TIMES WEEKLY") || textUpper.contains("TIMES A WEEK"))
					{
						repeat.setPeriodUnits(UnitsOfTime.WK);
					}
				}
			}
			else if (textUpper.startsWith("ONCE"))
			{
				repeat.setFrequency(1);
                repeat.setPeriod(BigDecimal.valueOf(1));
				repeat.setPeriodUnits(UnitsOfTime.D);
			}
			else if (textUpper.equals("AFTER BREAKFAST") || textUpper.equals("AFTER LUNCH") || textUpper.equals("AFTER DINNER"))
			{
				repeat.setFrequency(1);
                repeat.setPeriod(BigDecimal.valueOf(1));
				repeat.setPeriodUnits(UnitsOfTime.D);
			}
		}

		Period period = new Period();
		if (startDate != null)
		{
			period.setStart(startDate);
		}
		if (endDate != null)
		{
			period.setEnd(endDate);
		}
		repeat.setBounds(period);

		timingDtToReturn.setRepeat(repeat);

		return timingDtToReturn;
	}

	/**
	 * Limited functionality intended to be used only in this class.
	 * First attempts to get int after stripping away all non-numbers from string.
	 * If that doesn't work look for ONE through TEN.
	 * if that doesn't work returns - 1.
	 * @param string
	 * @return
	 */
	private static int getNumberFromString(String string)
	{
		int numToReturn = -1;
		if (string != null)
		{
			String textUpper = string.toUpperCase();
			String numberString = textUpper.replaceAll("\\D+",""); // strips out all non-numbers

			try
			{
				numToReturn = Integer.parseInt(numberString);
			}
			catch (Exception e)
			{
				// expected; ignore
			}

			if (numToReturn <= 0) // above didn't work; try to get with word search
			{
				if (textUpper.contains("ONE"))
				{
					numToReturn = 1;
				}
				else if (textUpper.contains("TWO"))
				{
					numToReturn = 2;
				}
				else if (textUpper.contains("TWO"))
				{
					numToReturn = 2;
				}
				else if (textUpper.contains("THREE"))
				{
					numToReturn = 3;
				}
				else if (textUpper.contains("FOUR"))
				{
					numToReturn = 4;
				}
				else if (textUpper.contains("FIVE"))
				{
					numToReturn = 5;
				}
				else if (textUpper.contains("SIX"))
				{
					numToReturn = 6;
				}
				else if (textUpper.contains("SEVEN"))
				{
					numToReturn = 7;
				}
				else if (textUpper.contains("EIGHT"))
				{
					numToReturn = 8;
				}
				else if (textUpper.contains("NINE"))
				{
					numToReturn = 9;
				}
				else if (textUpper.contains("TEN"))
				{
					numToReturn = 10;
				}
			}
		}
		return numToReturn;
	}

	/**
	 * Returns map of captions to content
	 * @param itemEntity		XML with a repeat of <caption/><content/> enttiies.
	 * @return
	 */
	private static HashMap<String, String> getUnsignedOrderCaptionToContentMap(XmlEntity itemEntity)
	{
		HashMap<String, String> mapToReturn = new HashMap<String, String>();
		ArrayList<XmlEntity> itemChildEntityList = itemEntity.getChildren();
		for (int k = 0; k < itemChildEntityList.size(); k++)
		{
			mapToReturn.put(itemChildEntityList.get(k).getValue(), itemChildEntityList.get(k + 1).getValue());
		}
		return mapToReturn;
	}

	/**
	 * Returns true if the DateTime or Period occurred prior to evalTime.
	 * Returns false if it did not.
	 * Returns true or false if the answer is "maybe", depending on the countMayeAsTrue variable.
	 *
	 * Pre-condition: assumes evalTime is a "real" date-time, without need to consider ambiguities in the time it represents (e.g., "sometime in 2015").
	 *
	 * @param dateTimeDtOrPeriodDt
	 * @param evalTime
	 * @param countMaybeAsTrue
	 * @return
	 */
	public static boolean isDateTimeOrPeriodBefore21(org.hl7.fhir.dstu3.model.Type dateTimeDtOrPeriodDt, org.hl7.fhir.dstu3.model.DateTimeType evalTime, boolean countMaybeAsTrue)
	{
		if ((dateTimeDtOrPeriodDt != null) && (evalTime != null))
		{
			boolean isDateTime = false;
			boolean isPeriod = false;
			org.hl7.fhir.dstu3.model.DateTimeType dateTimeType;
			org.hl7.fhir.dstu3.model.Period period;
			try
			{
				dateTimeType = (org.hl7.fhir.dstu3.model.DateTimeType) dateTimeDtOrPeriodDt;
				isDateTime = true;
			}
			catch (Exception e)
			{
			}
			if (! isDateTime)
			{
				try
				{
					period = (org.hl7.fhir.dstu3.model.Period) dateTimeDtOrPeriodDt;
					isPeriod = true;
				}
				catch (Exception e)
				{
				}
			}
			if (isDateTime)
			{
				// TODO: actually implement.  For now, just return true.
				// define the upper and lower range of time this represents
				return true;
			}
			else if (isPeriod)
			{
				// TODO: actually implement.  For now, just return true.
				// define the upper and lower range of time this represents
				return true;
			}
		}

		return false;
	}

	/**
	 * Returns true if the DateTime or Period occurred prior to evalTime.
	 * Returns false if it did not.
	 * Returns true or false if the answer is "maybe", depending on the countMayeAsTrue variable.
	 *
	 * Pre-condition for now: assumes time2 is a "real" date-time, without need to consider ambiguities in the time it represents (e.g., "sometime in 2015").
	 * Also assume that dateTimeOrPeriod1 is in fact a dateTimeType with a "real" date-time.
	 * TODO: update above pre-condition to be more generic
	 *
	 * @param dateTimeOrPeriod1
	 * @param time2
	 * @param maxAmountTime1BeforeTime2
	 * @param timeUnits
	 * @param countMaybeAsTrue
	 * @return
	 */
	public static boolean isDateTimeOrPeriodBeforeByAtMost21(org.hl7.fhir.dstu3.model.Type dateTimeOrPeriod1, DateTimeType time2, int maxAmountTime1BeforeTime2, int timeUnits, boolean countMaybeAsTrue)
	{
		org.hl7.fhir.dstu3.model.DateTimeType time1 = null;
		try
		{
			time1 = (org.hl7.fhir.dstu3.model.DateTimeType) dateTimeOrPeriod1;
		}
		catch (Exception e)
		{
			return false;
		}
		if ((dateTimeOrPeriod1 != null) && (time2 != null) && (time1 != null))
		{
			java.util.Date date1 = time1.getValue();
			java.util.Date date2 = time2.getValue();

			if ((date1 != null) && (date2 != null))
			{
				if (date1.before(date2))
				{
					java.util.Date date2MinusInterval = DateUtility.getInstance().getDateAfterAddingTime(date2, timeUnits, -1 * maxAmountTime1BeforeTime2);

					if (! date1.before(date2MinusInterval))
					{
						return true;
					}
				}
			}

		}

		return false;
	}

	/**
	 * Returns true if the subjectResourceReferenceDt points to the patient and he/she has an identifier that matches the focalPatientId
	 * @param subjectResourceReference
	 * @param patient
	 * @param focalPatientId
	 * @return
	 */
	public static boolean isFocalPatient(Reference subjectResourceReference, Patient patient, Identifier focalPatientId)
	{
		if ((subjectResourceReference != null) && (subjectResourceReference.getReference() != null) && (subjectResourceReference.getReference() != null) && (patient != null) && (patient.getId() != null) && (patient.getId() != null))
		{
			if (patient.getId().equals(subjectResourceReference.getReference()))
			{
				if (Utils.isIdentifierListContains(patient.getIdentifier(), focalPatientId))
				{
					return true;
				}
			}

		}
		return false;
	}

	/**
	 * Returns true if the subjectResourceReferenceDt points to the patient and he/she has an identifier that matches the focalPatientId
	 * @param subjectResourceReferenceDt
	 * @param patient
	 * @param focalPatientId
	 * @return
	 */
	public static boolean isFocalPatient21(org.hl7.fhir.dstu3.model.Reference subjectResourceReferenceDt, org.hl7.fhir.dstu3.model.Patient patient, org.hl7.fhir.dstu3.model.Identifier focalPatientId)
	{
		if ((subjectResourceReferenceDt != null) && (subjectResourceReferenceDt.getReference() != null) && (subjectResourceReferenceDt.getReference() != null) && (patient != null) && (patient.getId() != null) && (patient.getId() != null))
		{
			if (patient.getId().equals(subjectResourceReferenceDt.getReference()))
			{
				if (Utils.isIdentifierListContains21(patient.getIdentifier(), focalPatientId))
				{
					return true;
				}
			}

		}
		return false;
	}

	/**
	 * focalPersonId does not need to contain "Patient/"; if not, added before processing.
	 * @param patientReference
	 * @param focalPersonId
	 * @return
	 */
    public static boolean isReferencedPatientIdFocalPersonId(Reference patientReference, String focalPersonId) {
        log.debug("Patient Reference" + patientReference + ", focalPersonId: " + focalPersonId);
        if ((patientReference != null) && (focalPersonId != null)) {
            String patientReferenceStr = patientReference.getReferenceElement().getIdPart();
            if (patientReferenceStr != null) {
                // account for reference to patient that uses absolute path to
                // rest service
                if (focalPersonId.equals(patientReferenceStr)) {
                    return true;
                }
            }
        }

        return false;
    }

	/**
	 * Returns true if dateTime contained within at least one of medOrder's DosageInstructions.Timing.Repeat.BoundsPeriod
	 * @param dateTime
	 * @param medOrder
	 * @return
	 */
	public static boolean isDateTimeWithinMedicationOrderTimingPeriod(Date dateTime, MedicationOrder medOrder)
	{
		if ((dateTime != null) && (medOrder != null))
		{
			List<MedicationOrderDosageInstructionComponent> dosageInstructionList = medOrder.getDosageInstruction();
			if (dosageInstructionList != null)
			{
				for (MedicationOrderDosageInstructionComponent dosageInstruction : dosageInstructionList)
				{
					Timing timing = dosageInstruction.getTiming();
					if (timing != null)
					{
						TimingRepeatComponent repeat = timing.getRepeat();
						if (repeat != null)
						{
							// TODO: may need to support other types of bounds
							try
							{
								Period boundsPeriod = repeat.getBoundsPeriod();
								if (isDateTimeWithinPeriod(dateTime, boundsPeriod))
								{
									return true;
								}

							} catch (Exception e)
							{
								// would not work if not a period
							}
						}
					}
				}
			}
		}

		return false;
	}

	/**
	 * Returns true if:
	 * - start OR end is present
	 * - if start present, dateTime is on or after start
	 * - if end present, dateTime is on or before end
	 * @param dateTime
	 * @param period
	 * @return
	 */
	public static boolean isDateTimeWithinPeriod(Date dateTime, Period period)
	{
		if ((dateTime != null) && (period != null))
		{
			Date start = period.getStart();
			Date end = period.getEnd();

			if ((start != null) || (end != null))
			{
				boolean startNullOrConditionPassed = false;
				boolean endNullOrConditionPassed = false;

				if (start == null)
				{
					startNullOrConditionPassed = true;
				}
				else
				{
					if (! dateTime.before(start))
					{
						startNullOrConditionPassed = true;
					}
				}

				if (end == null)
				{
					endNullOrConditionPassed = true;
				}
				else
				{
					if (! dateTime.after(end))
					{
						endNullOrConditionPassed = true;
					}
				}

				if ((startNullOrConditionPassed) && (endNullOrConditionPassed))
				{
					return true;
				}
			}
		}
		return false;
	}

	public static CommunicationRequest createPayload(String message)
	{
		CommunicationRequest comRequest = new CommunicationRequest();
		CommunicationRequestPayloadComponent payload = comRequest.addPayload();
		StringType stringType = new  StringType(message);
		payload.setContent(stringType);
		return comRequest;
	}

	public static org.hl7.fhir.dstu3.model.CommunicationRequest createPayload3(String message)
	{
		org.hl7.fhir.dstu3.model.CommunicationRequest comRequest = new org.hl7.fhir.dstu3.model.CommunicationRequest();
		org.hl7.fhir.dstu3.model.CommunicationRequest.CommunicationRequestPayloadComponent payload = comRequest.addPayload();
		org.hl7.fhir.dstu3.model.StringType stringType = new  org.hl7.fhir.dstu3.model.StringType(message);
		payload.setContent(stringType);
		return comRequest;
	}

	// function Provenance createProvenance(String message) {
	// 		Provenance prov = new Provenance();
	// 		Agent agent = prov.addAgent();
	// 		IDatatype stringType = new  StringDt(message);
	//         	agent.setDisplay(message);
	// return prov;
	// }

	/**
	 * Returns true if and only if cc is contained within targetCcs in terms of system and code
	 * @param cc
	 * @param targetCcs
	 * @return
	 */
	public static Boolean isCodingContains(CodeableConcept cc, List<CodeableConcept> targetCcs){
		if (targetCcs != null)
		{
			for (CodeableConcept targetCc : targetCcs)
			{
				List<Coding> targetL =  targetCc.getCoding();
		        for (Coding targetCodingDt : targetL) {
		        	String targetSystem = targetCodingDt.getSystem();
		        	String targetCode = targetCodingDt.getCode();

		        	if ((targetSystem != null) && (targetCode != null))
		        	{
		        		Boolean thisResult = isCodingContains(cc, targetSystem, targetCode);
		        		if (thisResult.booleanValue() == true)
		        		{
		        			return true;
		        		}
		        	}
				}
			}
		}

		return false;
	}

	public static Boolean isCodingContains(CodeableConcept cc, String system, String code){
		 Boolean result = false;
		 List<Coding> l =  cc.getCoding();
         for (Coding codingDt : l) {
			if(codingDt.getSystem().equalsIgnoreCase(system) &&
				codingDt.getCode().equalsIgnoreCase(code)){
				result = true;
			}
		}
         return result;
	}

	public static Boolean isCodingContains(List<CodeableConcept> ccList, String system, String code){
		 Boolean result = false;
		 for (CodeableConcept cc : ccList)
			{
				if (isCodingContains(cc, system, code)){
					result = true;
				}
			}
			return result;
	}



	public static Boolean isCodingContains(CodeableConcept cc, String system, Set<String> codes)
	{
		 Boolean result = false;
		 List<Coding> l =  cc.getCoding();
        for (Coding codingDt : l) {
			if(codingDt.getSystem().equalsIgnoreCase(system) &&
				codes.contains(codingDt.getCode()))
			{
				result = true;
			}
		}
        return result;
	}

	public static Boolean isCodingContains(List<CodeableConcept> ccList, String system, Set<String> codes)
	{
		Boolean result = false;
	 	for (CodeableConcept cc : ccList)
		{
			if (isCodingContains(cc, system, codes)){
				result = true;
			}
		}
		return result;
	}





	public static Boolean isCodingContains(CodeableConcept cc, String system, String[] codes)
	{
		 Boolean result = false;
		 List<Coding> l =  cc.getCoding();
        for (Coding codingDt : l) {
			if(codingDt.getSystem().equalsIgnoreCase(system) &&
					ArrayUtils.contains(codes, codingDt.getCode()))
			{
				result = true;
			}
		}
        return result;
	}

	public static Boolean isCodingContains(List<CodeableConcept> ccList, String system, String[] codes)
	{
		Boolean result = false;
	 	for (CodeableConcept cc : ccList)
		{
			if (isCodingContains(cc, system, codes)){
				result = true;
			}
		}
		return result;
	}



	public static Boolean isCodingContains3(org.hl7.fhir.dstu3.model.CodeableConcept cc, String system, String code){
		 Boolean result = false;
		 List<org.hl7.fhir.dstu3.model.Coding> l =  cc.getCoding();
        for (org.hl7.fhir.dstu3.model.Coding codingDt : l) {
			if(codingDt.getSystem().equalsIgnoreCase(system) &&
				codingDt.getCode().equalsIgnoreCase(code)){
				result = true;
			}
		}
        return result;
	}

	public static Boolean isCodingContains3(List<org.hl7.fhir.dstu3.model.CodeableConcept> ccList, String system, String code){
		Boolean result = false;
	 	for (org.hl7.fhir.dstu3.model.CodeableConcept cc : ccList)
		{
			if (isCodingContains3(cc, system, code)){
				result = true;
			}
		}
		return result;
	}





	public static Boolean isCodingContains3(org.hl7.fhir.dstu3.model.CodeableConcept cc, String system, String[] codes){
		 Boolean result = false;
		 List<org.hl7.fhir.dstu3.model.Coding> l =  cc.getCoding();
		 for (org.hl7.fhir.dstu3.model.Coding codingDt : l) {
			if(codingDt.getSystem().equalsIgnoreCase(system) &&
			   ArrayUtils.contains(codes, codingDt.getCode())){
				result = true;
			}
		 }
		 return result;
	}

	public static Boolean isCodingContains3(org.hl7.fhir.dstu3.model.CodeableConcept cc, String system, List<String> codes){
		Boolean result = false;
		List<org.hl7.fhir.dstu3.model.Coding> l =  cc.getCoding();
		for (org.hl7.fhir.dstu3.model.Coding codingDt : l) {
			if(codingDt.getSystem().equalsIgnoreCase(system) &&
					ArrayUtils.contains(codes.toArray(new String[0]), codingDt.getCode())){
				result = true;
			}
		}
		return result;
	}

	public static Boolean isCodingContains3(List<org.hl7.fhir.dstu3.model.CodeableConcept> ccList, String system, String[] codes){
		Boolean result = false;
		for (org.hl7.fhir.dstu3.model.CodeableConcept cc : ccList)
		{
			if (isCodingContains3(cc, system, codes)){
				result = true;
			}
		}
		return result;
	}

	public static Boolean isCodingContains3(List<org.hl7.fhir.dstu3.model.CodeableConcept> ccList, String system, List<String> codes){
		Boolean result = false;
		for (org.hl7.fhir.dstu3.model.CodeableConcept cc : ccList)
		{
			if (isCodingContains3(cc, system, codes.toArray(new String[0]))){
				result = true;
			}
		}
		return result;
	}

	// pre-condition: must match on non-null system and value.  Ignores case.
	public static Boolean isIdentifierListContains(List identifierList, Identifier identifier)
	{
		Boolean result = false;

		if((identifier != null) && (identifier.getSystem() != null) && (identifier.getValue() != null))
		{
			for (Identifier identifierListEntry : (List<Identifier>)identifierList)
			{
    			if((identifierListEntry != null) && (identifierListEntry.getSystem() != null) && (identifierListEntry.getValue() != null) &&
    			   (identifierListEntry.getSystem().equalsIgnoreCase(identifier.getSystem())) &&
    			   (identifierListEntry.getValue().equalsIgnoreCase(identifier.getValue())))
    				{
    				result = true;
    			}
    		}
		 }
		return true;
	}

	// pre-condition: must match on non-null system and value.  Ignores case.
	public static Boolean isIdentifierListContains21(List identifierList, org.hl7.fhir.dstu3.model.Identifier identifier)
	{
		Boolean result = false;

		if((identifier != null) && (identifier.getSystem() != null) && (identifier.getValue() != null))
		{
			for (org.hl7.fhir.dstu3.model.Identifier identifierListEntry : (List<org.hl7.fhir.dstu3.model.Identifier>)identifierList)
			{
    			if((identifierListEntry != null) && (identifierListEntry.getSystem() != null) && (identifierListEntry.getValue() != null) &&
    			   (identifierListEntry.getSystem().equalsIgnoreCase(identifier.getSystem())) &&
    			   (identifierListEntry.getValue().equalsIgnoreCase(identifier.getValue())))
    				{
    				result = true;
    			}
    		}
		 }
		return true;
	}

	public static Boolean isIdentifierListContains3(List identifierList, String id)
	{
		Boolean result = false;

		if(id != null)
		{
			for (org.hl7.fhir.dstu3.model.Identifier identifierListEntry : (List<org.hl7.fhir.dstu3.model.Identifier>)identifierList)
			{
    			if((identifierListEntry != null) && (identifierListEntry.getValue() != null) &&
    			   (identifierListEntry.getValue().equalsIgnoreCase(id)))
    				{
    				result = true;
    			}
    		}
		 }
		return true;
	}

	public static boolean isReferenceListContains(List<org.hl7.fhir.dstu3.model.Reference> referenceList, String id)
	{
		if((referenceList != null) && (id != null))
		{
			for (org.hl7.fhir.dstu3.model.Reference referenceListEntry : referenceList)
			{
				if(referenceListEntry.getReference() != null)
    			{
					if(referenceListEntry.getReference().equals(id))
	    			{
						return true;
	    			}
    			}
    		}
		 }
		return false;
	}


	/*
	 * Returns a person's age using the calendarUnits specified.  If unable to compute, returns -1.
	 *
	 * Time components are ignored if calendarTimeUnit is in years, months, or days.
	 *
	 * @param calendarTimeUnit - must be in java Calendar units.  Weeks are not supported.
	 *
	 */
	public static long getAgeInTimeUnitAtTime(java.util.Date birthDate, java.util.Date specifiedTime, int calendarTimeUnit)
	{
		if ((birthDate == null) || (specifiedTime == null))
		{
			return -1;
		}

		DateUtility dateUtility = DateUtility.getInstance();

		// this function ignores time components if time units are days or above
		AbsoluteTimeDifference atd = dateUtility.getAbsoluteTimeDifference(birthDate, specifiedTime, calendarTimeUnit);

		if (calendarTimeUnit == java.util.Calendar.YEAR)
		{
			return atd.getYearDifference();
		}
		else if (calendarTimeUnit == java.util.Calendar.MONTH)
		{
			return atd.getMonthDifference();
		}
		else if ((calendarTimeUnit == java.util.Calendar.DATE) ||
			(calendarTimeUnit == java.util.Calendar.DAY_OF_MONTH) ||
			(calendarTimeUnit == java.util.Calendar.DAY_OF_WEEK) ||
			(calendarTimeUnit == java.util.Calendar.DAY_OF_WEEK_IN_MONTH) ||
			(calendarTimeUnit == java.util.Calendar.DAY_OF_YEAR))
		{
			return atd.getDayDifference();
		}
		else if ((calendarTimeUnit == java.util.Calendar.HOUR_OF_DAY) ||
                (calendarTimeUnit == java.util.Calendar.HOUR))
		{
			return atd.getHourDifference();
		}
		else if (calendarTimeUnit == java.util.Calendar.MINUTE)
		{
			return atd.getMinuteDifference();
		}
		else if (calendarTimeUnit == java.util.Calendar.SECOND)
		{
			return atd.getSecondDifference();
		}
		else if (calendarTimeUnit == java.util.Calendar.MILLISECOND)
		{
			return atd.getMillisecondDifference();
		}
		else
		{
			return -1;
		}
	}

	public static class Assertion extends Object
	{
		private String myValue;

		public Assertion(String value)
		{
			myValue = value;
		}

		public String getValue()
		{
			return myValue;
		}

		public void setValue(String value)
		{
			myValue = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myValue == null) ? 0 : myValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Assertion other = (Assertion) obj;
			if (myValue == null) {
				if (other.myValue != null)
					return false;
			} else if (!myValue.equals(other.myValue))
				return false;
			return true;
		}
	}

	public static class EvalTime extends Object
	{
		private DateTimeType myValue;

		public EvalTime(DateTimeType value) {
			this.myValue = value;
		}

		public DateTimeType getValue() {
			return myValue;
		}

		public void setValue(DateTimeType value) {
			this.myValue = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myValue == null) ? 0 : myValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			EvalTime other = (EvalTime) obj;
			if (myValue == null) {
				if (other.myValue != null)
					return false;
			} else if (!myValue.equals(other.myValue))
				return false;
			return true;
		}
	}

	public static class EvalTime21 extends Object
	{
		private DateTimeType myValue;

		public EvalTime21(DateTimeType value) {
			this.myValue = value;
		}

		public DateTimeType getValue() {
			return myValue;
		}

		public void setValue(DateTimeType value) {
			this.myValue = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myValue == null) ? 0 : myValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			EvalTime other = (EvalTime) obj;
			if (myValue == null) {
				if (other.myValue != null)
					return false;
			} else if (!myValue.equals(other.myValue))
				return false;
			return true;
		}
	}

	public static class FocalPatientId extends Object
	{
		private Identifier myValue;

		public FocalPatientId(Identifier value) {
			this.myValue = value;
		}

		public Identifier getValue() {
			return myValue;
		}

		public void setValue(Identifier value) {
			this.myValue = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myValue == null) ? 0 : myValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {  // TODO: IdentifierDt does not implement equals; this may not work correctly until that is fixed
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FocalPatientId other = (FocalPatientId) obj;
			if (myValue == null) {
				if (other.myValue != null)
					return false;
			} else if (!myValue.equals(other.myValue))
				return false;
			return true;
		}
	}

	public static class FocalPatientId3 extends Object
	{
		private org.hl7.fhir.dstu3.model.Identifier myValue;

		public FocalPatientId3(org.hl7.fhir.dstu3.model.Identifier value) {
			this.myValue = value;
		}

		public org.hl7.fhir.dstu3.model.Identifier getValue() {
			return myValue;
		}

		public void setValue(org.hl7.fhir.dstu3.model.Identifier value) {
			this.myValue = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myValue == null) ? 0 : myValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {  // TODO: IdentifierDt does not implement equals; this may not work correctly until that is fixed
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FocalPatientId other = (FocalPatientId) obj;
			if (myValue == null) {
				if (other.myValue != null)
					return false;
			} else if (!myValue.equals(other.myValue))
				return false;
			return true;
		}
	}

	public static class ClientLanguage extends Object
	{
		private String myValue;

		public ClientLanguage(String value)
		{
			myValue = value;
		}

		public String getValue()
		{
			return myValue;
		}

		public void setValue(String value)
		{
			myValue = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myValue == null) ? 0 : myValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Assertion other = (Assertion) obj;
			if (myValue == null) {
				if (other.myValue != null)
					return false;
			} else if (!myValue.equals(other.myValue))
				return false;
			return true;
		}
	}

	public static class ClientTimeZoneOffset extends Object
	{
		private String myValue;

		public ClientTimeZoneOffset(String value)
		{
			myValue = value;
		}

		public String getValue()
		{
			return myValue;
		}

		public void setValue(String value)
		{
			myValue = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myValue == null) ? 0 : myValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Assertion other = (Assertion) obj;
			if (myValue == null) {
				if (other.myValue != null)
					return false;
			} else if (!myValue.equals(other.myValue))
				return false;
			return true;
		}
	}

	public static class FiredRule extends Object
	{
		private String myValue;

		public FiredRule(String value)
		{
			myValue = value;
		}

		public String getValue()
		{
			return myValue;
		}

		public void setValue(String value)
		{
			myValue = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myValue == null) ? 0 : myValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Assertion other = (Assertion) obj;
			if (myValue == null) {
				if (other.myValue != null)
					return false;
			} else if (!myValue.equals(other.myValue))
				return false;
			return true;
		}
	}

	public static class NamedInteger extends Object
	{
		private String myName;
		private Integer myInteger;

		public NamedInteger(String name, int integer) {
			this.myName = name;
			this.myInteger = Integer.valueOf(integer);
		}

		public String getName() {
			return myName;
		}
		public void setName(String name) {
			this.myName = name;
		}
		public Integer getInteger() {
			return myInteger;
		}
		public void setInteger(int integer) {
			this.myInteger = Integer.valueOf(integer);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myInteger == null) ? 0 : myInteger.hashCode());
			result = prime * result
					+ ((myName == null) ? 0 : myName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NamedInteger other = (NamedInteger) obj;
			if (myInteger == null) {
				if (other.myInteger != null)
					return false;
			} else if (!myInteger.equals(other.myInteger))
				return false;
			if (myName == null) {
				if (other.myName != null)
					return false;
			} else if (!myName.equals(other.myName))
				return false;
			return true;
		}
	}

	public static class NamedDouble extends Object
	{
		private String myName;
		private Double myDouble;

		public NamedDouble(String name, double inputDouble) {
			this.myName = name;
			this.myDouble = Double.valueOf(inputDouble);
		}

		public String getName() {
			return myName;
		}
		public void setName(String name) {
			this.myName = name;
		}
		public Double getDouble() {
			return myDouble;
		}
		public void setDouble(double inputDouble) {
			this.myDouble = Double.valueOf(inputDouble);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myDouble == null) ? 0 : myDouble.hashCode());
			result = prime * result
					+ ((myName == null) ? 0 : myName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NamedDouble other = (NamedDouble) obj;
			if (myDouble == null) {
				if (other.myDouble != null)
					return false;
			} else if (!myDouble.equals(other.myDouble))
				return false;
			if (myName == null) {
				if (other.myName != null)
					return false;
			} else if (!myName.equals(other.myName))
				return false;
			return true;
		}
	}

	public static class NamedBoolean extends Object
	{
		private String myName;
		private Boolean myBoolean;

		public NamedBoolean(String name, boolean bool) {
			this.myName = name;
			this.myBoolean = Boolean.valueOf(bool);
		}

		public String getName() {
			return myName;
		}
		public void setName(String name) {
			this.myName = name;
		}
		public Boolean getBool() {
			return myBoolean;
		}
		public void setBool(boolean bool) {
			this.myBoolean = Boolean.valueOf(bool);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myBoolean == null) ? 0 : myBoolean.hashCode());
			result = prime * result
					+ ((myName == null) ? 0 : myName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NamedBoolean other = (NamedBoolean) obj;
			if (myBoolean == null) {
				if (other.myBoolean != null)
					return false;
			} else if (!myBoolean.equals(other.myBoolean))
				return false;
			if (myName == null) {
				if (other.myName != null)
					return false;
			} else if (!myName.equals(other.myName))
				return false;
			return true;
		}
	}

	public static class NamedString extends Object
	{
		private String myName;
		private String myString;

		public NamedString(String name, String string)
		{
			this.myName = name;
			this.myString = string;
		}

		public String getName() {
			return myName;
		}
		public void setName(String name) {
			this.myName = name;
		}
		public String getString() {
			return myString;
		}
		public void setString(String string) {
			this.myString = string;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myName == null) ? 0 : myName.hashCode());
			result = prime * result
					+ ((myString == null) ? 0 : myString.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NamedString other = (NamedString) obj;
			if (myName == null) {
				if (other.myName != null)
					return false;
			} else if (!myName.equals(other.myName))
				return false;
			if (myString == null) {
				if (other.myString != null)
					return false;
			} else if (!myString.equals(other.myString))
				return false;
			return true;
		}
	}


	public static class NamedEncounter extends Object
	{
		private String myName;
		private Encounter myEncounter;

		public NamedEncounter(String name, Encounter encounter) {
			this.myName = name;
			this.myEncounter = encounter;
		}
		public String getName() {
			return myName;
		}
		public void setName(String name) {
			this.myName = name;
		}
		public Encounter getEncounter() {
			return myEncounter;
		}
		public void setEncounter(Encounter encounter) {
			this.myEncounter = encounter;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myEncounter == null) ? 0 : myEncounter.hashCode());
			result = prime * result
					+ ((myName == null) ? 0 : myName.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) { // TODO: Encounter does not implement equals; this may not work correctly until that is fixed
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NamedEncounter other = (NamedEncounter) obj;
			if (myEncounter == null) {
				if (other.myEncounter != null)
					return false;
			} else if (!myEncounter.equals(other.myEncounter))
				return false;
			if (myName == null) {
				if (other.myName != null)
					return false;
			} else if (!myName.equals(other.myName))
				return false;
			return true;
		}
	}

	public static class NamedList extends Object
	{
		private String myName;
		private List myList;

		public NamedList(String name, List list) {
			this.myName = name;
			this.myList = list;
		}
		public String getName() {
			return myName;
		}
		public void setName(String name) {
			this.myName = name;
		}
		public List getList() {
			return myList;
		}
		public void setList(List list) {
			this.myList = list;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myList == null) ? 0 : myList.hashCode());
			result = prime * result
					+ ((myName == null) ? 0 : myName.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NamedList other = (NamedList) obj;
			if (myList == null) {
				if (other.myList != null)
					return false;
			} else if (!myList.equals(other.myList))
				return false;
			if (myName == null) {
				if (other.myName != null)
					return false;
			} else if (!myName.equals(other.myName))
				return false;
			return true;
		}
	}

	public static class NamedDateTimeDt extends Object
	{
		private String myName;
		private DateTimeType myDateTimeDt;

		public NamedDateTimeDt(String name, DateTimeType dateTimeDt)
		{
			myName = name;
			myDateTimeDt = dateTimeDt;
		}

		public String getName() {
			return myName;
		}

		public void setName(String name) {
			this.myName = name;
		}

		public DateTimeType getDateTimeDt() {
			return myDateTimeDt;
		}

		public void setDateTimeDt(DateTimeType dateTimeDt) {
			this.myDateTimeDt = dateTimeDt;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((myDateTimeDt == null) ? 0 : myDateTimeDt.hashCode());
			result = prime * result
					+ ((myName == null) ? 0 : myName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NamedDateTimeDt other = (NamedDateTimeDt) obj;
			if (myDateTimeDt == null) {
				if (other.myDateTimeDt != null)
					return false;
			} else if (!myDateTimeDt.equals(other.myDateTimeDt))
				return false;
			if (myName == null) {
				if (other.myName != null)
					return false;
			} else if (!myName.equals(other.myName))
				return false;
			return true;
		}
	}

	public static class NamedIdentifierDt extends Object
	{
		private String myName;
		private Identifier myIdentifierDt;

		public String getName() {
			return myName;
		}
		public void setName(String name) {
			this.myName = name;
		}
		public Identifier getIdentifierDt() {
			return myIdentifierDt;
		}
		public void setIdentifierDt(Identifier identifierDt) {
			this.myIdentifierDt = identifierDt;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime
					* result
					+ ((myIdentifierDt == null) ? 0 : myIdentifierDt.hashCode());
			result = prime * result
					+ ((myName == null) ? 0 : myName.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) { // TODO: IdentifierDt does not implement equals; this may not work correctly until that is fixed
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NamedIdentifierDt other = (NamedIdentifierDt) obj;
			if (myIdentifierDt == null) {
				if (other.myIdentifierDt != null)
					return false;
			} else if (!myIdentifierDt.equals(other.myIdentifierDt))
				return false;
			if (myName == null) {
				if (other.myName != null)
					return false;
			} else if (!myName.equals(other.myName))
				return false;
			return true;
		}
	}

	public static void writeToFile(String outputPath, String output)
	{
		writeToFile(outputPath, new ByteArrayInputStream(output.getBytes()));
	}

	public static void writeToFile(String outputPath, InputStream inputStream)
	{
		  OutputStream outputStream = null;

			try {
				// write the inputStream to a FileOutputStream
				outputStream = new FileOutputStream(new File(outputPath));

				int read = 0;
				byte[] bytes = new byte[1024];

				while ((read = inputStream.read(bytes)) != -1) {
					outputStream.write(bytes, 0, read);
				}
		 	} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (inputStream != null) {
					try {
						inputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (outputStream != null) {
					try {
						// outputStream.flush();
						outputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}

				}
			}
	}

	public static void writeToFile(String outputPath, ArrayList<String> outputLines)
	{
		  StringBuffer outputStringBuffer = new StringBuffer();;
		  for (int k = 0; k < outputLines.size(); k++)
		  {
			  outputStringBuffer.append(outputLines.get(k));
			  if (k != outputLines.size() - 1)
			  {
				  outputStringBuffer.append("\n");
			  }
		  }
		  InputStream stream = new ByteArrayInputStream(outputStringBuffer.toString().getBytes(StandardCharsets.UTF_8));
		  writeToFile(outputPath, stream);
	}

	/*
	public static void updateMedicationOrderWithStructuredComponentsUpdatedWithFreeText(MedicationOrder medOrder, boolean override)
	{
		if (medOrder != null)
		{

		}
	}
	*/

	/**
	 * Used to tell if medication order is missing dose quantity.  At least in some EHRs, indicates that this was a free-text sig without structured Sig components.
	 * @param medOrder
	 * @return
	 */
    public static boolean isMedicationOrderMissingDoseQuantity(MedicationOrder medOrder) {
        if (medOrder == null) {
            return true;
        }
        return !medOrder.getDosageInstruction().stream().anyMatch(di -> di.getDose() != null);
    }

	public static void setAttribute(String id, String attributeName, Object attributeValue, Map<String,Map<String,Object>> idToAttributeNameValuePairMap)
	{
		if (idToAttributeNameValuePairMap != null)
		{
			Map<String,Object> nameValuePair = idToAttributeNameValuePairMap.get(id);
			if (nameValuePair == null)
			{
				nameValuePair = new HashMap<String, Object>();
			}
			nameValuePair.put(attributeName, attributeValue);

			idToAttributeNameValuePairMap.put(id, nameValuePair);
		}
		else
		{
			System.err.println("idToAttributeNameValuePairMap is null.");
		}
	}

	/**
	 * Return a map with following elements:
	 * endDate (Date)
	 * endDatePreferDefault (Date) -- end date where the if methods MaxDailyDoseInXXX, ExpectedSupplyDurationFromInput, or DispenseQtyAndUseFrequency are used, instead uses DefaultRxDaysSupplyEstimate.
	 * 								 Included because for checking if a patient has been on an Rx for a specified period of time, this estimate may be more accurate (when the intent is for prn meds to be
	 * 								 only taken at a lower dose with, e.g., a presumed 30d supply).
	 * endDateCalcMethod (String)
	 * 		EndDateInSig
	 * 		EndDateInNote
	 * 		DurationInSig
	 * 		DurationInNote
	 * 		AverageDailyDoseInSig
	 * 		AverageDailyDoseInNote
	 * 		MaxDailyDoseInSig
	 * 		MaxDailyDoseInNote
	 * 		ExpectedSupplyDurationFromInput						Pre-calculated from structured sigs for some EHRs
	 * 		DispenseQtyAndUseFrequency
	 * 		DefaultRxDaysSupplyEstimate
	 * @param startDate
	 * @param numRepeatsAllowed
	 * @param dispenseQuantity
	 * @param expectedSupplyDuration
	 * @param rxSig_sig
	 * @param rxSig_note
	 * @param defaultRxDaysSupplyEstimate
	 * @param numTimesPerDayFromStructuredData_mayBeNull
	 * @param doseValueFromStructuredData_mayBeNull
	 * @param doseUnitFromStructuredData_mayBeNull
	 * @return
	 */
	public static Map<String, Object> estimateMedOrderEndDate(Date startDate, Integer numRepeatsAllowed, SimpleQuantity dispenseQuantity, Duration expectedSupplyDuration, RxSig rxSig_sig, RxSig rxSig_note, int defaultRxDaysSupplyEstimate, Double numTimesPerDayFromStructuredData_mayBeNull,
			Double doseValueFromStructuredData_mayBeNull, String doseUnitFromStructuredData_mayBeNull)
	{
		Date endDate = null;
		String endDateCalcMethod = null;

		int refills = 0;

		if (startDate != null)
		{
			if (numRepeatsAllowed != null)
			{
				refills = numRepeatsAllowed.intValue();
			}

			// 0a. end date in sig and not before start date
			if ((endDate == null) && (rxSig_sig != null))
			{
				Date endDateFromRxSig = rxSig_sig.getEndDate();
				if (endDateFromRxSig != null)
				{
					if (! endDateFromRxSig.before(startDate))
					{
						endDate = endDateFromRxSig;
						endDateCalcMethod = "EndDateInSig";
					}
				}
			}

			// 0b. end date in note and not before start date
			if ((endDate == null) && (rxSig_note != null))
			{
				Date endDateFromRxSig = rxSig_note.getEndDate();
				if (endDateFromRxSig != null)
				{
					if (! endDateFromRxSig.before(startDate))
					{
						endDate = endDateFromRxSig;
						endDateCalcMethod = "EndDateInNote";
					}
				}
			}

			// 1. days supply in sig
			if ((endDate == null) && (rxSig_sig != null))
			{
				Integer duration = rxSig_sig.getDuration();
				String durationUnits = rxSig_sig.getDurationTimeUnits();

				int calendarTimeUnits = getCalendarTimeUnitsFromString(durationUnits);
				if ((duration != null) && (calendarTimeUnits != -1))
				{
					endDate = DateUtility.getInstance().getDateAfterAddingTime(startDate, calendarTimeUnits, (duration.intValue() * (1 + refills)));
					endDateCalcMethod = "DurationInSig";
				}
			}

			// 2. days supply in note
			if ((endDate == null) && (rxSig_note != null))
			{
				Integer duration = rxSig_note.getDuration();
				String durationUnits = rxSig_note.getDurationTimeUnits();

				int calendarTimeUnits = getCalendarTimeUnitsFromString(durationUnits);
				if ((duration != null) && (calendarTimeUnits != -1))
				{
					endDate = DateUtility.getInstance().getDateAfterAddingTime(startDate, calendarTimeUnits, (duration.intValue() * (1 + refills)));
					endDateCalcMethod = "DurationInNote";
				}
			}

			// 3a. ave/d in sig and dispense quantity in structured data
			if ((endDate == null) && (rxSig_sig != null))
			{
				if (dispenseQuantity != null)
				{
					BigDecimal dispenseNum = dispenseQuantity.getValue();
					String dispenseUnits = dispenseQuantity.getUnit();
					String averageDoseUnits = rxSig_sig.getAverageDailyDoseUnits();
					Double averageDailyDose = rxSig_sig.getAverageDailyDose();

					boolean dosingAndDispenseUnitsMatchOrNull = Utils.isDosingAndDispenseUnitsMatchOrNull(averageDoseUnits, dispenseUnits);

					if ((dosingAndDispenseUnitsMatchOrNull) && (averageDailyDose != null) && (dispenseNum != null))
					{
						int duration = (int) Math.ceil(dispenseNum.doubleValue() / averageDailyDose.doubleValue());
						endDate = DateUtility.getInstance().getDateAfterAddingTime(startDate, Calendar.DATE, (duration * (1 + refills)));
						endDateCalcMethod = "AverageDailyDoseInSig";
					}
				}
			}

			// 3b. ave/d in note and dispense quantity in structured data
			if ((endDate == null) && (rxSig_note != null))
			{
				if (dispenseQuantity != null)
				{
					BigDecimal dispenseNum = dispenseQuantity.getValue();
					String dispenseUnits = dispenseQuantity.getUnit();
					String averageDoseUnits = rxSig_note.getAverageDailyDoseUnits();
					Double averageDailyDose = rxSig_note.getAverageDailyDose();

					boolean dosingAndDispenseUnitsMatchOrNull = Utils.isDosingAndDispenseUnitsMatchOrNull(averageDoseUnits, dispenseUnits);

					if ((dosingAndDispenseUnitsMatchOrNull) && (averageDailyDose != null) && (dispenseNum != null))
					{
						int duration = (int) Math.ceil(dispenseNum.doubleValue() / averageDailyDose.doubleValue());
						endDate = DateUtility.getInstance().getDateAfterAddingTime(startDate, Calendar.DATE, (duration * (1 + refills)));
						endDateCalcMethod = "AverageDailyDoseInNote";
					}
				}
			}

			// 3c. max/d in sig and dispense quantity in structured data
			if ((endDate == null) && (rxSig_sig != null))
			{
				if (dispenseQuantity != null)
				{
					BigDecimal dispenseNum = dispenseQuantity.getValue();
					String dispenseUnits = dispenseQuantity.getUnit();
					String maxDoseUnits = rxSig_sig.getMaxDailyDoseUnits();
					Double maxDailyDose = rxSig_sig.getMaxDailyDose();

					boolean dosingAndDispenseUnitsMatchOrNull = Utils.isDosingAndDispenseUnitsMatchOrNull(maxDoseUnits, dispenseUnits);

					if ((dosingAndDispenseUnitsMatchOrNull) && (maxDailyDose != null) && (dispenseNum != null))
					{
						int duration = (int) Math.ceil(dispenseNum.doubleValue() / maxDailyDose.doubleValue());
						endDate = DateUtility.getInstance().getDateAfterAddingTime(startDate, Calendar.DATE, (duration * (1 + refills)));
						endDateCalcMethod = "MaxDailyDoseInSig";
					}
				}
			}

			// 3d. max/d in note and dispense quantity in structured data
			if ((endDate == null) && (rxSig_note != null))
			{
				if (dispenseQuantity != null)
				{
					BigDecimal dispenseNum = dispenseQuantity.getValue();
					String dispenseUnits = dispenseQuantity.getUnit();
					String maxDoseUnits = rxSig_note.getMaxDailyDoseUnits();
					Double maxDailyDose = rxSig_note.getMaxDailyDose();

					boolean dosingAndDispenseUnitsMatchOrNull = Utils.isDosingAndDispenseUnitsMatchOrNull(maxDoseUnits, dispenseUnits);

					if ((dosingAndDispenseUnitsMatchOrNull) && (maxDailyDose != null) && (dispenseNum != null))
					{
						int duration = (int) Math.ceil(dispenseNum.doubleValue() / maxDailyDose.doubleValue());
						endDate = DateUtility.getInstance().getDateAfterAddingTime(startDate, Calendar.DATE, (duration * (1 + refills)));
						endDateCalcMethod = "MaxDailyDoseInNote";
					}
				}
			}

			// 4. estimated days supply from input
			if ((endDate == null) && (expectedSupplyDuration != null))
			{
				BigDecimal duration = expectedSupplyDuration.getValue();
				String durationUnitCode = expectedSupplyDuration.getCode();
				int calendarTimeUnits = getCalendarTimeUnitsFromString(durationUnitCode);

				if ((duration != null) && (calendarTimeUnits != -1))
				{
					endDate = DateUtility.getInstance().getDateAfterAddingTime(startDate, calendarTimeUnits, (duration.intValue() * (1 + refills)));
					endDateCalcMethod = "ExpectedSupplyDurationFromInput";
				}
			}

			// 5. dispense quantity and use frequency from structured data
			if ((endDate == null) && (numTimesPerDayFromStructuredData_mayBeNull != null) && (doseValueFromStructuredData_mayBeNull != null) && (doseUnitFromStructuredData_mayBeNull != null))
			{
				if (dispenseQuantity != null)
				{
					BigDecimal dispenseNum = dispenseQuantity.getValue();
					String dispenseUnits = dispenseQuantity.getUnit();

					String doseUnits = doseUnitFromStructuredData_mayBeNull;
					double doseHigh = doseValueFromStructuredData_mayBeNull.doubleValue();

					boolean dosingAndDispenseUnitsMatchOrNull = Utils.isDosingAndDispenseUnitsMatchOrNull(doseUnits, dispenseUnits);

					if (dosingAndDispenseUnitsMatchOrNull)
					{
						double expectedDurationInDays = dispenseNum.doubleValue() / numTimesPerDayFromStructuredData_mayBeNull.doubleValue() / doseHigh;
						if (expectedDurationInDays > 0)
						{
							endDate = DateUtility.getInstance().getDateAfterAddingTime(startDate, Calendar.DATE, (int) (Math.ceil(expectedDurationInDays) * (1 + refills)));
							endDateCalcMethod = "DispenseQtyAndUseFrequency";
						}
					}
				}
			}

			/** This isn't needed as if it wasn't a caution, structured data already would have been updated with the sig info.

			// 5b. dispense quantity and use frequency from Sig without caution warning
			if ((endDate == null) && (rxSig_sig != null))
			{
				if (dispenseQuantity != null)
				{
					BigDecimal dispenseNum = dispenseQuantity.getValue();
					String dispenseUnits = dispenseQuantity.getUnit();

					if (rxSig_sig.useWithCaution() == false)
					{
						Double doseHigh = rxSig_sig.getDoseHigh();
						if (doseHigh == null)
						{
							doseHigh = Double.valueOf (1.0);
						}
						String doseUnits = rxSig_sig.getDoseUnits();

						Integer frequencyHighObj = rxSig_sig.getFrequencyHigh();
						int frequencyHigh = 1;
						if (frequencyHighObj != null)
						{
							frequencyHigh = frequencyHighObj.intValue();
						}

						Integer intervalLow = rxSig_sig.getIntervalLow();
						String intervalUnits = rxSig_sig.getIntervalTimeUnits();

						boolean dosingAndDispenseUnitsMatchOrNull = Utils.isDosingAndDispenseUnitsMatchOrNull(doseUnits, dispenseUnits);

						if (dosingAndDispenseUnitsMatchOrNull)
						{
							double expectedDurationInDays = dispenseNum.doubleValue() / (getNumTimesPerDay(frequencyHigh, intervalLow.doubleValue(), getUnitsOfTimeFromStr(intervalUnits)) / doseHigh);
							if (expectedDurationInDays > 0)
							{
								endDate = DateUtility.getInstance().getDateAfterAddingTime(startDate, Calendar.DATE, (int) (Math.ceil(expectedDurationInDays) * (1 + refills)));
								endDateCalcMethod = "DispenseQtyAndUseFrequencyFromSigWithoutCaution";
							}
						}
					}
				}
			}
			*/

			// 6. default days
			if (endDate == null)
			{
				endDate = DateUtility.getInstance().getDateAfterAddingTime(startDate, Calendar.DATE, defaultRxDaysSupplyEstimate * (1 + refills));
				endDateCalcMethod = "DefaultRxDaysSupplyEstimate";
			}
		}

		Map<String, Object> mapToReturn = new HashMap<String, Object>();
		mapToReturn.put("endDate", endDate);
		mapToReturn.put("endDateCalcMethod", endDateCalcMethod);

		Date endDatePreferDefault = endDate;
		if ((endDateCalcMethod != null) && (endDateCalcMethod.startsWith("MaxDailyDose") || endDateCalcMethod.equals("ExpectedSupplyDurationFromInput") ||  endDateCalcMethod.equals("DispenseQtyAndUseFrequency")))
		{
			endDatePreferDefault = DateUtility.getInstance().getDateAfterAddingTime(startDate, Calendar.DATE, defaultRxDaysSupplyEstimate * (1 + refills));
		}
		mapToReturn.put("endDatePreferDefault", endDatePreferDefault);

		return mapToReturn;
	}

	/**
	 * Return true if doseUnits or dispenseUnits null, or if one contains the other.
	 * @param doseUnits
	 * @param dispenseUnits
	 * @return
	 */
	private static boolean isDosingAndDispenseUnitsMatchOrNull(String doseUnits, String dispenseUnits)
	{
		boolean dosingAndDispenseUnitsMatchOrNull = false;
		if ((doseUnits == null) || (dispenseUnits == null))
		{
			dosingAndDispenseUnitsMatchOrNull = true;
		}
		else
		{
			String doseUnitsLower = doseUnits.toLowerCase();
			String dispenseUnitsLower = dispenseUnits.toLowerCase();
			if (doseUnitsLower.startsWith(dispenseUnitsLower) || dispenseUnitsLower.startsWith(doseUnitsLower)) // to account for different abbreviations, e.g., tab vs. tablet, or tab vs. ""
			{
				dosingAndDispenseUnitsMatchOrNull = true;
			}
		}
		return dosingAndDispenseUnitsMatchOrNull;
	}


	public static double getNumTimesPerDay(int frequency, double period, UnitsOfTime periodUnits)
	{
		double numTimesPerDay = 0;

		if (period > 0)
		{
			if (periodUnits.equals(UnitsOfTime.H)) // hr
			{
				numTimesPerDay = frequency * (24.0 / period);
			}
			else if (periodUnits.equals(UnitsOfTime.MIN)) // minutes
			{
				numTimesPerDay = frequency * (24.0 / period) * 60;
			}
			else if (periodUnits.equals(UnitsOfTime.D)) // day
			{
				numTimesPerDay = frequency * (24.0 / period) / 24;
			}
			else if (periodUnits.equals(UnitsOfTime.WK)) // week
			{
				numTimesPerDay = frequency * (24.0 / period) / (24 * 7);
			}
			else if (periodUnits.equals(UnitsOfTime.MO)) // month
			{
				numTimesPerDay = frequency * (24.0 / period) / (24 * 30); // assuming 30 days in month
			}
		}
		return numTimesPerDay;
	}

	/**
	 * Returns -1 if no match.
	 * @param timeUnits
	 * @return
	 */
	public static int getCalendarTimeUnitsFromString(String timeUnits)
	{
		UnitsOfTime unitsOfTime = getUnitsOfTimeFromStr(timeUnits);
		if (unitsOfTime != null)
		{
			if (unitsOfTime == UnitsOfTime.A)
			{
				return Calendar.YEAR;
			}
			else if (unitsOfTime == UnitsOfTime.MO)
			{
				return Calendar.MONTH;
			}
			else if (unitsOfTime == UnitsOfTime.WK)
			{
				return Calendar.WEEK_OF_YEAR;
			}
			else if (unitsOfTime == UnitsOfTime.D)
			{
				return Calendar.DATE;
			}
			else if (unitsOfTime == UnitsOfTime.H)
			{
				return Calendar.HOUR_OF_DAY;
			}
			else if (unitsOfTime == UnitsOfTime.MIN)
			{
				return Calendar.MINUTE;
			}
			else if (unitsOfTime == UnitsOfTime.S)
			{
				return Calendar.SECOND;
			}
		}
		return -1;
	}

	public static Object getAttributeValue(String id, String attributeName, Map<String,Map<String,Object>> idToAttributeNameValuePairMap)
	{
		if ((id != null) && (attributeName != null) && (idToAttributeNameValuePairMap != null))
		{
			Map<String, Object> nameValuePair = idToAttributeNameValuePairMap.get(id);
			if (nameValuePair != null)
			{
				return nameValuePair.get(attributeName);
			}
		}
		return null;
	}

	public static void reverseMistakenLabelingOfMedOrderStartAndEndDatesWithUtcTimeZone(MedicationOrder medOrder, TimeZone correctTimeZone)
	{
		if (medOrder != null)
		{
			List<MedicationOrderDosageInstructionComponent> dosageInstructionList = medOrder.getDosageInstruction();
			if (dosageInstructionList != null)
			{
				for (MedicationOrderDosageInstructionComponent dosageInstruction : dosageInstructionList)
				{
					Timing timing = dosageInstruction.getTiming();
					if (timing != null)
					{
						TimingRepeatComponent repeat = timing.getRepeat();
						if (repeat != null)
						{
							try
							{
								Period period = repeat.getBoundsPeriod();
								if (period != null)
								{
									Date start = period.getStart();
									if (start != null)
									{
										period.setStart(getDateReversingMistakenLabelingOfDateWithUtcTimeZone(start, correctTimeZone));
									}
									Date end = period.getEnd();
									if (end != null)
									{
										period.setEnd(getDateReversingMistakenLabelingOfDateWithUtcTimeZone(end, correctTimeZone));
									}
								}
							}
							catch (Exception e)
							{
								// do nothing
							}
						}
					}
				}
			}

			MedicationOrderDispenseRequestComponent dispenseRequest = medOrder.getDispenseRequest();
			if (dispenseRequest != null)
			{
				Period period = dispenseRequest.getValidityPeriod();
				if (period != null)
				{
					Date start = period.getStart();
					if (start != null)
					{
						period.setStart(getDateReversingMistakenLabelingOfDateWithUtcTimeZone(start, correctTimeZone));
					}
					Date end = period.getEnd();
					if (end != null)
					{
						period.setEnd(getDateReversingMistakenLabelingOfDateWithUtcTimeZone(end, correctTimeZone));
					}
				}
			}
		}
	}

	/**
	 * Taking in a date where the time zone was mistakenly labeled as UTC, reverses the error.  Needed to adjust for some EHRs making this error.
	 * @param dateWithTimeZoneMistakenlyLabeledAsUtc
	 * @param correctTimeZone
	 */
	public static Date getDateReversingMistakenLabelingOfDateWithUtcTimeZone(Date dateWithTimeZoneMistakenlyLabeledAsUtc, TimeZone correctTimeZone)
	{
		if (dateWithTimeZoneMistakenlyLabeledAsUtc == null)
		{
			return null;
		}

		int offset = correctTimeZone.getOffset(dateWithTimeZoneMistakenlyLabeledAsUtc.getTime())/1000/60/60;
		return DateUtility.getInstance().getDateAfterAddingTime(dateWithTimeZoneMistakenlyLabeledAsUtc, Calendar.HOUR, -1 * offset);
	}

	public static void main(String[] args)
	{
		// test getDateReversingMistakenLabelingOfDateWithUtcTimeZone (confirmed works across daylight savings time adjustments around 3/12/17
		Calendar utcCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		utcCalendar.clear();
		utcCalendar.set(2017, 3-1, 13);
		Date originalDate = utcCalendar.getTime();
		System.out.println(originalDate);
	    System.out.println(Utils.getDateReversingMistakenLabelingOfDateWithUtcTimeZone(originalDate, TimeZone.getDefault()));
	}
}
