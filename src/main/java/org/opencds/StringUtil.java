package org.opencds;

public class StringUtil {

    public static boolean notNullOrEmpty(String str) {
        return str != null && !str.isEmpty();
    }
}
