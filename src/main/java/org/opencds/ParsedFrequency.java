package org.opencds;

public class ParsedFrequency {
    private Integer frequencyLow;
    private Integer frequencyHigh;
    private String frequencyTimeUnits;
    private Boolean isPrn;
    private String regexPattern;

    public ParsedFrequency() {
    }

    public Integer getFrequencyLow() {
        return frequencyLow;
    }

    public void setFrequencyLow(Integer frequencyLow) {
        this.frequencyLow = frequencyLow;
    }

    public Integer getFrequencyHigh() {
        return frequencyHigh;
    }

    public void setFrequencyHigh(Integer frequencyHigh) {
        this.frequencyHigh = frequencyHigh;
    }

    public String getFrequencyTimeUnits() {
        return frequencyTimeUnits;
    }

    public void setFrequencyTimeUnits(String frequencyTimeUnits) {
        this.frequencyTimeUnits = frequencyTimeUnits;
    }

    public Boolean getPrn() {
        return isPrn;
    }

    public void setPrn(Boolean prn) {
        isPrn = prn;
    }

    public String getRegexPattern() {
        return regexPattern;
    }

    public void setRegexPattern(String regexPattern) {
        this.regexPattern = regexPattern;
    }

}
