package org.opencds;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IntervalParser {

    public static ParsedInterval fromLeftoverSig(String leftoverSig) {
        // q_X-Y_h(r) pattern
        // group                  1        4       5          6         9                       10
        //                      2   3                      7    8                11+
        // example                 q      (_)      4      (-6)         (_)      hr/h/month/week/day (terminal or prn - assume hr)
        String intervalPattern = "((^q)|( q))([\\s]?)([\\d]+)((-)([\\d]+))?([\\s]?)((hr$)|(h$)|(hr )|(h )|(month)|" +
                "(week)|(day)|($)|(prn))";

        ParsedInterval parsedInterval = new ParsedInterval();
        parsedInterval.setRegexPattern(intervalPattern);

        Pattern pattern = Pattern.compile(intervalPattern);
        Matcher matcher = pattern.matcher(leftoverSig);

        if (matcher.find()) {
            String intervalLow = matcher.group(5);
            String intervalHigh = matcher.group(6);
            String intervalUnits = matcher.group(10);
            if (intervalUnits != null) {
                intervalUnits = intervalUnits.trim();
            }

            try {
                parsedInterval.setIntervalLow(Integer.valueOf(intervalLow));
                if ((intervalHigh != null) && (!intervalHigh.isEmpty())) {
                    parsedInterval.setIntervalHigh(Integer.valueOf(intervalHigh.substring(1))); // remove leading '-'
                } else {
                    parsedInterval.setIntervalHigh(parsedInterval.getIntervalLow());
                }

                if (intervalUnits != null) {
                    if ((intervalUnits.isEmpty()) || (intervalUnits.startsWith("h"))) {
                        parsedInterval.setIntervalTimeUnits("h");
                    } else if (intervalUnits.equals("prn")) {
                        parsedInterval.setIntervalTimeUnits("h");
                        parsedInterval.setPrn(true);
                    } else if ((intervalUnits.equals("day"))) {
                        parsedInterval.setIntervalTimeUnits("d");
                    } else {
                        parsedInterval.setIntervalTimeUnits(intervalUnits);
                    }
                }
                return parsedInterval;
            } catch (Exception e) {
                System.err.println("> Unable to convert " + leftoverSig + " to an interval"
                        + ".  Identified text: " + matcher.group(0) + ".  Leftover SIG: " + leftoverSig);
            }
        }

        return null;
    }

}
