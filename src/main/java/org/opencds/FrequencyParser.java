package org.opencds;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FrequencyParser {


    public static ParsedFrequency fromLeftoverSig(String leftoverSig) {
        // todo
        // group              1   2     3               4                        11                  12
        //        				                5      6      7         10                 13+
        //                  					               8    9
        //example            ^  (prn)  (_)     (   2    (x) (-    3)      x )   (_)    (every day/every night/every morning/prn/$)
        String frequencyPattern = "(^)(prn)?([\\s]?)(([\\d]+)(x?)((-)([\\d]+))?(x))?([\\s]?)((every day$)|" +
                "(every day )|(every night$)|(every night )|(every morning$)|(every morning )|(prn$)|(prn )|($))";

        ParsedFrequency parsedFrequency = new ParsedFrequency();
        parsedFrequency.setRegexPattern(frequencyPattern);

        Pattern pattern = Pattern.compile(frequencyPattern);
        Matcher matcher = pattern.matcher(leftoverSig);

        if (matcher.find()) {
            if (StringUtil.notNullOrEmpty(matcher.group(0))) {
                String frequency = matcher.group(4);
                String frequencyLow = matcher.group(5);
                String frequencyHigh = matcher.group(9);
                String frequencyUnits = matcher.group(12);
                if (frequencyUnits != null) {
                    frequencyUnits = frequencyUnits.trim();
                }

                if ((frequency != null) || ((frequencyUnits != null) && (frequencyUnits.startsWith("every")))) {

                    try {
                        if (StringUtil.notNullOrEmpty(frequencyLow)) {
                            parsedFrequency.setFrequencyLow(Integer.valueOf(frequencyLow));
                        } else {
                            parsedFrequency.setFrequencyLow(1);
                        }

                        if (StringUtil.notNullOrEmpty(frequencyHigh)) {
                            parsedFrequency.setFrequencyHigh(Integer.valueOf(frequencyHigh));
                        } else {
                            parsedFrequency.setFrequencyHigh(parsedFrequency.getFrequencyLow());
                        }

                        if (StringUtil.notNullOrEmpty(frequencyUnits)) {
                            if ((frequencyUnits.startsWith("every day")) ||
                                    (frequencyUnits.startsWith("every night")) ||
                                    (frequencyUnits.startsWith("every morning"))) {
                                parsedFrequency.setFrequencyTimeUnits("d");
                            } else if (frequencyUnits.startsWith("prn")) {
                                parsedFrequency.setPrn(true);
                                parsedFrequency.setFrequencyTimeUnits("d");
                            }
                        } else {
                            parsedFrequency.setFrequencyTimeUnits("d");
                        }
                        return parsedFrequency;
                    } catch (Exception e) {
                        System.err.println("> Unable to convert " + leftoverSig + " to a frequency");
                    }

                }
            }
        }

        return null;
    }

}
