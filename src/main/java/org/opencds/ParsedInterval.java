package org.opencds;

public class ParsedInterval {
    private Integer intervalLow;
    private Integer intervalHigh;
    private String intervalTimeUnits;
    private Boolean isPrn;
    private String regexPattern;

    public Integer getIntervalLow() {
        return intervalLow;
    }

    public void setIntervalLow(Integer intervalLow) {
        this.intervalLow = intervalLow;
    }

    public Integer getIntervalHigh() {
        return intervalHigh;
    }

    public void setIntervalHigh(Integer intervalHigh) {
        this.intervalHigh = intervalHigh;
    }

    public String getIntervalTimeUnits() {
        return intervalTimeUnits;
    }

    public void setIntervalTimeUnits(String intervalTimeUnits) {
        this.intervalTimeUnits = intervalTimeUnits;
    }

    public Boolean getPrn() {
        return isPrn;
    }

    public void setPrn(Boolean prn) {
        isPrn = prn;
    }

    public String getRegexPattern() {
        return regexPattern;
    }

    public void setRegexPattern(String regexPattern) {
        this.regexPattern = regexPattern;
    }
}
