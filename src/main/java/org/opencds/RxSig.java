package org.opencds;

import org.opencds.common.utilities.DateUtility;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RxSig {
    private DateUtility dateUtility = DateUtility.getInstance();

    private String myOriginalSig;
    private String myNormalizedSig;
    private String myCommand;
    private Double myDoseLow;
    private Double myDoseHigh;
    private String myDoseUnits;
    private Double myAlternateDoseLow;
    private Double myAlternateDoseHigh;
    private String myAlternateDoseUnits;
    private String myRoute;
    private Integer myFrequencyLow;
    private Integer myFrequencyHigh;
    private String myFrequencyTimeUnits;
    private Integer myIntervalLow;
    private Integer myIntervalHigh;
    private String myIntervalTimeUnits;
    private String myLeftoverSig;
    private boolean myIsPrn;
    private String myIndication;
    private Integer myDuration;
    private String myDurationTimeUnits;
    private String myComments;
    private String myIgnorableSig;
    private Date myEarliestFillDate;
    private Date myStartDate;
    private Date myEndDate;
    private Integer myMaxDailyFrequency;
    private Double myMaxDailyDose;
    private String myMaxDailyDoseUnits;
    private Integer myAverageDailyFrequency;
    private Double myAverageDailyDose;
    private String myAverageDailyDoseUnits;

    private boolean myDurationAlreadyParsed = false; // to ensure only gets parse attempt once

    public RxSig(String originalSig) {
        myOriginalSig = originalSig;
        myIsPrn = false;
        processSig();
    }

    /**
     * If, after removing dates, leftover sig contains numbers or "and", returns true.
     *
     * @return
     */
    public boolean useWithCaution() {
        if (myLeftoverSig == null) {
            return false;
        } else {
            String strWithoutDates = new String(myLeftoverSig);
            strWithoutDates.replaceAll("([\\d]+)(/)([\\d]+)(/)([\\d]+)", "");
            if (strWithoutDates.matches("(.)*(\\d)(.)*")) {
                return true;
            }
            if (strWithoutDates.contains("and") || strWithoutDates.contains("starting at") || strWithoutDates.contains("beginning at")) {
                return true;
            }
            return false;
        }
    }

    public boolean isDoseRanged() {
        if (myDoseLow != null) {
            if ((myDoseHigh != null) && (myDoseHigh.doubleValue() != myDoseLow.doubleValue())) {
                return true;
            }
        }
        return false;
    }

    public String getOriginalSig() {
        return myOriginalSig;
    }

    public String getNormalizedSig() {
        return myNormalizedSig;
    }

    public String getCommand() {
        return myCommand;
    }

    public Double getDoseLow() {
        return myDoseLow;
    }

    public Double getDoseHigh() {
        return myDoseHigh;
    }

    public String getDoseUnits() {
        return myDoseUnits;
    }

    public Double getAlternateDoseLow() {
        return myAlternateDoseLow;
    }

    public Double getAlternateDoseHigh() {
        return myAlternateDoseHigh;
    }

    public String getAlternateDoseUnits() {
        return myAlternateDoseUnits;
    }

    public String getRoute() {
        return myRoute;
    }

    public Integer getFrequencyLow() {
        return myFrequencyLow;
    }

    public Integer getFrequencyHigh() {
        return myFrequencyHigh;
    }

    /**
     * The frequency units represent the timeframe within which a specific event,
     * such as taking medication, will occur. For instance, in the instruction
     * 'TAKE 1 BY MOUTH TWICE DAILY,' here frequency unit would be 'd' indicating
     * that the event occurs twice per day. These frequency units correspond to
     * what FHIR refers to as period units, as defined in the FHIR
     * <a href="https://www.hl7.org/fhir/datatypes.html#Timing_">Timing</a> data type.
     */
    public String getFrequencyTimeUnits() {
        return myFrequencyTimeUnits;
    }

    public Integer getIntervalLow() {
        return myIntervalLow;
    }

    public Integer getIntervalHigh() {
        return myIntervalHigh;
    }

    public String getIntervalTimeUnits() {
        return myIntervalTimeUnits;
    }

    public String getLeftoverSig() {
        return myLeftoverSig;
    }

    public boolean isPrn() {
        return myIsPrn;
    }

    public String getIndication() {
        return myIndication;
    }

    public Integer getDuration() {
        return myDuration;
    }

    public String getDurationTimeUnits() {
        return myDurationTimeUnits;
    }

    public String getComments() {
        return myComments;
    }

    public String getIgnorableSig() {
        return myIgnorableSig;
    }

    public Date getEarliestFillDate() {
        return myEarliestFillDate;
    }

    public Date getStartDate() {
        return myStartDate;
    }

    public Date getEndDate() {
        return myEndDate;
    }

    public Integer getMaxDailyFrequency() {
        return myMaxDailyFrequency;
    }

    public Double getMaxDailyDose() {
        return myMaxDailyDose;
    }

    public String getMaxDailyDoseUnits() {
        return myMaxDailyDoseUnits;
    }

    public Integer getAverageDailyFrequency() {
        return myAverageDailyFrequency;
    }

    public Double getAverageDailyDose() {
        return myAverageDailyDose;
    }

    public String getAverageDailyDoseUnits() {
        return myAverageDailyDoseUnits;
    }

    private void processSig() {
        String sig = "" + myOriginalSig;

        //----------------------------
        //----------------------------
        // get normalized sig
        //----------------------------
        //----------------------------

        // lower case all and remove \n
        myNormalizedSig = sig.toLowerCase().replaceAll("\n", " ");

        // ends in ", pain"
        if (myNormalizedSig.endsWith(", pain")) {
            myNormalizedSig = myNormalizedSig.substring(0, myNormalizedSig.length() - 6);
            myIndication = "pain";
        }

        // remove !, *, :, ;   replace with space
        myNormalizedSig = myNormalizedSig.replaceAll("[!*:;]", " ");

        // remove commas
        myNormalizedSig = myNormalizedSig.replaceAll("[,]", "");

        // remove please, the, of, with
        myNormalizedSig = myNormalizedSig.replaceAll("please", "");
        myNormalizedSig = myNormalizedSig.replaceAll(" the ", " ");
        myNormalizedSig = myNormalizedSig.replaceAll(" of ", " ");
        myNormalizedSig = myNormalizedSig.replaceAll(" with ", " ");

        // convert all multi-hyphens to single hyphen
        while (myNormalizedSig.contains("--")) {
            myNormalizedSig = myNormalizedSig.replace("--", "-");
        }

        while (myNormalizedSig.contains("- -")) {
            myNormalizedSig = myNormalizedSig.replace("- -", "-");
        }

        //---------------------------
        // numbers
        //---------------------------
        myNormalizedSig = myNormalizedSig.replaceAll("two and half", "2.5");
        myNormalizedSig = myNormalizedSig.replaceAll("two and a half", "2.5");
        myNormalizedSig = myNormalizedSig.replaceAll("two and one half", "2.5");
        myNormalizedSig = myNormalizedSig.replaceAll("two and one-half", "2.5");
        myNormalizedSig = myNormalizedSig.replaceAll("2 and 1/2", "2.5");
        myNormalizedSig = myNormalizedSig.replaceAll("one and half", "1.5");
        myNormalizedSig = myNormalizedSig.replaceAll("one and a half", "1.5");
        myNormalizedSig = myNormalizedSig.replaceAll("one and one half", "1.5");
        myNormalizedSig = myNormalizedSig.replaceAll("one and one-half", "1.5");
        myNormalizedSig = myNormalizedSig.replaceAll("1 1/2", "1.5");
        myNormalizedSig = myNormalizedSig.replaceAll("1 and 1/2", "1.5");
        myNormalizedSig = myNormalizedSig.replaceAll("1 half", "0.5");
        myNormalizedSig = myNormalizedSig.replaceAll("one half", "0.5");
        myNormalizedSig = myNormalizedSig.replaceAll("one-half", "0.5");
        myNormalizedSig = myNormalizedSig.replaceAll("half", "0.5");
        myNormalizedSig = myNormalizedSig.replaceAll("1/2 ", "0.5 ");
        myNormalizedSig = myNormalizedSig.replaceAll("1/2-1", "0.5-1");
        myNormalizedSig = myNormalizedSig.replaceAll("1/2- 1", "0.5-1");
        myNormalizedSig = myNormalizedSig.replaceAll("1/2- - 1", "0.5-1");
        myNormalizedSig = myNormalizedSig.replaceAll("una ", "1 ");
        myNormalizedSig = myNormalizedSig.replaceAll("one ", "1 ");
        myNormalizedSig = myNormalizedSig.replaceAll("one-", "1-");
        myNormalizedSig = myNormalizedSig.replaceAll("one full ", "1 ");
        myNormalizedSig = myNormalizedSig.replaceAll("oxycod1 ", "oxycodone ");
        myNormalizedSig = myNormalizedSig.replaceAll("roxycod1 ", "roxycodone ");
        myNormalizedSig = myNormalizedSig.replaceAll("roxicod1 ", "roxycodone ");
        myNormalizedSig = myNormalizedSig.replaceAll("hydrocod1 ", "hydrocodone ");
        myNormalizedSig = myNormalizedSig.replaceAll("hydromorph1 ", "hydromorphone ");
        myNormalizedSig = myNormalizedSig.replaceAll("two", "2");
        myNormalizedSig = myNormalizedSig.replaceAll("three", "3");
        myNormalizedSig = myNormalizedSig.replaceAll("four", "4");
        myNormalizedSig = myNormalizedSig.replaceAll("five", "5");
        myNormalizedSig = myNormalizedSig.replaceAll("six", "6");
        myNormalizedSig = myNormalizedSig.replaceAll("seven", "7");
        myNormalizedSig = myNormalizedSig.replaceAll("eight", "8");
        myNormalizedSig = myNormalizedSig.replaceAll("nine", "9");
        myNormalizedSig = myNormalizedSig.replaceAll(" ten", " 10");
        myNormalizedSig = myNormalizedSig.replaceAll("eleven", "11");
        myNormalizedSig = myNormalizedSig.replaceAll("twleve", "12");
        myNormalizedSig = myNormalizedSig.replace("1 (1)", "1");
        myNormalizedSig = myNormalizedSig.replace("2 (2)", "2");
        myNormalizedSig = myNormalizedSig.replace("3 (3)", "3");
        myNormalizedSig = myNormalizedSig.replace("4 (4)", "4");
        myNormalizedSig = myNormalizedSig.replace("5 (5)", "5");
        myNormalizedSig = myNormalizedSig.replace("6 (6)", "6");
        myNormalizedSig = myNormalizedSig.replace("7 (7)", "7");
        myNormalizedSig = myNormalizedSig.replace("8 (8)", "8");
        myNormalizedSig = myNormalizedSig.replace("9 (9)", "9");
        myNormalizedSig = myNormalizedSig.replace("10 (10)", "10");
        myNormalizedSig = myNormalizedSig.replace("11 (11)", "11");
        myNormalizedSig = myNormalizedSig.replace("12 (12)", "12");

        //													   1                5          6          10                11
        //                                                2      3       4               7   8    9               12       13    14
        //                                               1       (.)    (5)      (_)       to/-/or      (_)       2       (.)    (5)
        myNormalizedSig = myNormalizedSig.replaceAll("(([\\d]+)([\\.]?)([\\d]*))([\\s]*)((to)|(-)|(or))([\\s]*)(([\\d]+)([\\.]?)([\\d]*))", "$1-$11");

        myNormalizedSig = myNormalizedSig.replaceAll("times", "x");
        myNormalizedSig = myNormalizedSig.replaceAll("([\\d]+)([\\s]*)([x])", "$1x");
        myNormalizedSig = myNormalizedSig.replaceAll("una vez", "1x");
        myNormalizedSig = myNormalizedSig.replaceAll("once", "1x");
        myNormalizedSig = myNormalizedSig.replaceAll("1 time", "1x");
        myNormalizedSig = myNormalizedSig.replaceAll("twice", "2x");
        myNormalizedSig = myNormalizedSig.replaceAll(" bid", " 2x");
        myNormalizedSig = myNormalizedSig.replaceAll("bid ", "2x ");
        myNormalizedSig = myNormalizedSig.replaceAll("thrice", "3x");
        myNormalizedSig = myNormalizedSig.replaceAll(" tid", " 3x");
        myNormalizedSig = myNormalizedSig.replaceAll("tid ", "3x ");
        myNormalizedSig = myNormalizedSig.replaceAll(" qid", " 4x");
        myNormalizedSig = myNormalizedSig.replaceAll("qid ", "4x ");

        myNormalizedSig = myNormalizedSig.replaceAll("([\\d]+)([x]?)(-)([\\d]+)([x])", "$1-$4x");

        // clean
        myNormalizedSig = getCleanedStr(myNormalizedSig);

        // extract out earliest fill dates
        processEarliestFillDate("earliest fill date");
        processEarliestFillDate("to be filled on");
        processEarliestFillDate("to be filled on or after");
        processEarliestFillDate("ok to fill");
        processEarliestFillDate("may fill");

        myNormalizedSig = getCleanedStr(myNormalizedSig);

        // extract out use dates
        processUseDates("for use dates");
        processUseDates("use dates");
        processUseDates("for use");
        processUseDates("dates");
        processUseDates("use");

        myNormalizedSig = getCleanedStr(myNormalizedSig);

        //---------------------------
        // normalize different ways to say the same thing
        //---------------------------


        //---------------------------
        // shorthands
        //---------------------------

        // q
        myNormalizedSig = myNormalizedSig.replaceAll("every", "q");
        myNormalizedSig = myNormalizedSig.replaceAll(" ever ", " q ");
        myNormalizedSig = myNormalizedSig.replaceAll(" evey ", " q ");

        //---------------------------
        // dose units
        //---------------------------

        // tab
        myNormalizedSig = myNormalizedSig.replaceAll("tablets", "tab");
        myNormalizedSig = myNormalizedSig.replaceAll("tableta", "tab");
        myNormalizedSig = myNormalizedSig.replaceAll("tablet", "tab");
        myNormalizedSig = myNormalizedSig.replaceAll("tabs", "tab");
        myNormalizedSig = myNormalizedSig.replaceAll("full tab", "tab");

        // pill
        myNormalizedSig = myNormalizedSig.replaceAll("pills", "pill");

        // capsule
        myNormalizedSig = myNormalizedSig.replaceAll("capsules", "capsule");

        // puff
        myNormalizedSig = myNormalizedSig.replaceAll("puffs", "puff");

        // tsp
        myNormalizedSig = myNormalizedSig.replaceAll("teaspoonfulls", "tsp");
        myNormalizedSig = myNormalizedSig.replaceAll("teaspoonfuls", "tsp");
        myNormalizedSig = myNormalizedSig.replaceAll("teaspoons", "tsp");
        myNormalizedSig = myNormalizedSig.replaceAll("teaspoon", "tsp");
        myNormalizedSig = myNormalizedSig.replaceAll("tsps", "tsp");
        myNormalizedSig = myNormalizedSig.replaceAll("tspfull", "tsp");
        myNormalizedSig = myNormalizedSig.replaceAll("tspful", "tsp");

        // patch
        myNormalizedSig = myNormalizedSig.replaceAll("patches", "patch");
        myNormalizedSig = myNormalizedSig.replaceAll("apply patch", "apply 1 patch"); // special case

        // film
        myNormalizedSig = myNormalizedSig.replaceAll("films", "film");

        // mg, mcg, ml, cc
        myNormalizedSig = myNormalizedSig.replaceAll(" mgs ", " mg ");
        myNormalizedSig = myNormalizedSig.replaceAll(" mcgs ", " mcg ");
        myNormalizedSig = myNormalizedSig.replaceAll(" mls ", " ml ");
        myNormalizedSig = myNormalizedSig.replaceAll(" ccs ", " cc ");

        //---------------------------
        // time units
        //---------------------------

        // hr
        myNormalizedSig = myNormalizedSig.replaceAll("hours", "hr");
        myNormalizedSig = myNormalizedSig.replaceAll("hour", "hr");
        myNormalizedSig = myNormalizedSig.replaceAll("hrs", "hr");

        // day
        myNormalizedSig = myNormalizedSig.replaceAll("days", "day");

        // week
        myNormalizedSig = myNormalizedSig.replaceAll("weeks", "week");
        myNormalizedSig = myNormalizedSig.replaceAll(" wks", " week");
        myNormalizedSig = myNormalizedSig.replaceAll(" wk", " week");

        // month
        myNormalizedSig = myNormalizedSig.replaceAll("months", "month");
        myNormalizedSig = myNormalizedSig.replaceAll(" mos", " month");
        myNormalizedSig = myNormalizedSig.replaceAll(" mo ", " month ");

        //---------------------------
        // sig phrases
        //---------------------------

        // prn
        myNormalizedSig = myNormalizedSig.replace("only as needed", "prn");
        myNormalizedSig = myNormalizedSig.replace("as needed", "prn");
        myNormalizedSig = myNormalizedSig.replace("as need ", "prn ");
        myNormalizedSig = myNormalizedSig.replace("only if needed", "prn");
        myNormalizedSig = myNormalizedSig.replace("if needed", "prn");
        myNormalizedSig = myNormalizedSig.replace("as necessary", "prn");
        myNormalizedSig = myNormalizedSig.replace("prn prn", "prn");

        //---------------------------
        // routes
        //---------------------------

        // PO
        myNormalizedSig = myNormalizedSig.replace("by mouths", "po");
        myNormalizedSig = myNormalizedSig.replace("by mouth", "po");
        myNormalizedSig = myNormalizedSig.replace("orally", "po");
        myNormalizedSig = myNormalizedSig.replace("via oral", "po");
        myNormalizedSig = myNormalizedSig.replace("po po", "po");

        // sublinqual
        myNormalizedSig = myNormalizedSig.replace("sublingual", "sl");
        myNormalizedSig = myNormalizedSig.replace("under tongue", "sl");

        // feeding tube
        myNormalizedSig = myNormalizedSig.replace("per feeding tube", "feeding tube");

        // skin
        myNormalizedSig = myNormalizedSig.replace("externally to the skin", "skin");
        myNormalizedSig = myNormalizedSig.replace("externally to skin", "skin");
        myNormalizedSig = myNormalizedSig.replace("to the skin", "skin");
        myNormalizedSig = myNormalizedSig.replace("to skin", "skin");
        myNormalizedSig = myNormalizedSig.replace(" and change", "");
        myNormalizedSig = myNormalizedSig.replace(" to be applied", "");

        // every day
        myNormalizedSig = myNormalizedSig.replace("daily", "every day");
        myNormalizedSig = myNormalizedSig.replace("todos los dias", "every day");
        myNormalizedSig = myNormalizedSig.replace(" a day", " every day");
        myNormalizedSig = myNormalizedSig.replace("al dia", "every day");
        myNormalizedSig = myNormalizedSig.replace("a diario", "every day");
        myNormalizedSig = myNormalizedSig.replace("per day", "every day");
        myNormalizedSig = myNormalizedSig.replace(" qd", " every day");
        myNormalizedSig = myNormalizedSig.replace(" q day", " every day");

        // every afternoon
        myNormalizedSig = myNormalizedSig.replace("in afternoon", "every afternoon");

        // every night/evening/bedtime
        myNormalizedSig = myNormalizedSig.replace(" q pm", " every night");
        myNormalizedSig = myNormalizedSig.replace(" q hs", " every night");
        myNormalizedSig = myNormalizedSig.replace(" q night", " every night");
        myNormalizedSig = myNormalizedSig.replace(" q evening", " every night");
        myNormalizedSig = myNormalizedSig.replace(" qhs", " every night");
        myNormalizedSig = myNormalizedSig.replace(" qpm", " every night");
        myNormalizedSig = myNormalizedSig.replace(" hs", " every night");
        myNormalizedSig = myNormalizedSig.replace(" pm", " every night");
        myNormalizedSig = myNormalizedSig.replace(" in pm", " every night");
        myNormalizedSig = myNormalizedSig.replace("at night", "every night");
        myNormalizedSig = myNormalizedSig.replace("every nighttime", "every night");
        myNormalizedSig = myNormalizedSig.replace("nightly", "every night");
        myNormalizedSig = myNormalizedSig.replace("at bedtime", "every night");
        myNormalizedSig = myNormalizedSig.replace("at bed time", "every night");
        myNormalizedSig = myNormalizedSig.replace("bed time", "every night");
        myNormalizedSig = myNormalizedSig.replace("before bed", "every night");
        myNormalizedSig = myNormalizedSig.replace("every night every night", "every night");
        myNormalizedSig = myNormalizedSig.replace("every day every night", "every night");

        // every_morning
        myNormalizedSig = myNormalizedSig.replace(" qam", " every morning");
        myNormalizedSig = myNormalizedSig.replace(" q am", " every morning");
        myNormalizedSig = myNormalizedSig.replace(" am", " every morning");
        myNormalizedSig = myNormalizedSig.replace(" q morning", " every morning");
        myNormalizedSig = myNormalizedSig.replace("in the morning", "every morning");
        myNormalizedSig = myNormalizedSig.replace("in morning", "every morning");
        myNormalizedSig = myNormalizedSig.replace("in am", "every morning");

        // combos
        myNormalizedSig = myNormalizedSig.replace("every day every morning", "every morning");
        myNormalizedSig = myNormalizedSig.replace("every morning every day", "every morning");
        myNormalizedSig = myNormalizedSig.replace("every morning and every night", "bid");

        myNormalizedSig = getCleanedStr(myNormalizedSig);

        // remove a, an
        myNormalizedSig = myNormalizedSig.replaceAll(" a ", " ");
        myNormalizedSig = myNormalizedSig.replaceAll(" an ", " ");

        // excess hyphen in some sigs
        myNormalizedSig = myNormalizedSig.replaceAll("([\\d]+)([\\s]?)(-)([\\s]?)(\\D)", "$1$2$4$5");
        myNormalizedSig = myNormalizedSig.replaceAll("  ", " ");

        //----------------------------
        //----------------------------
        // extract out known elements
        //----------------------------
        //----------------------------

        myLeftoverSig = myNormalizedSig;

        // ignorable phrases
        processIgnorablePhrase("do not combine other tylenol products");
        processIgnorablePhrase("can take stool softener if constipation occurs");
        processIgnorablePhrase("may cause drowsiness");
        processIgnorablePhrase("as it may sedate");
        processIgnorablePhrase("as it is sedating");
        processIgnorablePhrase("may sedate");
        processIgnorablePhrase("may fill early for travel");
        processIgnorablePhrase("do not drive on medication");
        processIgnorablePhrase("must be seen for further refills");
        processIgnoreablePhraseWithRegex("(may )(re)?(fil)(.+)(before use date)(s?)");
        processIgnoreablePhraseWithRegex("(may )(re)?(fil)(.+)(prior to use date)(s?)");
        processIgnoreablePhraseWithRegex("(may )(re)?(fil)(.+)(day prior)");
        processIgnoreablePhraseWithRegex("(may )(re)?(fil)(.+)(fill date)");
        processIgnoreablePhraseWithRegex("(may )(re)?(fil)(.+)(same day)");
        processIgnoreablePhraseWithRegex("(may )(re)?(fil)(.+)(today)");
        processIgnorablePhrase("not valid without seal");
        processIgnorablePhrase("bring to appointment");
        processIgnoreablePhraseWithRegex("(needs)(.+)(brand)");
        processIgnorablePhrase("this is a");
        processIgnorablePhrase("this is");
        processIgnorablePhrase("no early refills");
        processIgnorablePhrase("do not fill early");
        processIgnorablePhrase("attending to sign");
        processIgnorablePhrase("no further refills from pmc");
        processIgnorablePhrase("use as directed");
        processIgnorablePhrase("as directed");
        processIgnorablePhrase("wean asap");
        processIgnorablePhrase("and change");
        processIgnorablePhrase("change");
        processIgnorablePhrase("avoid with driving or work");
        processIgnorablePhrase("try to minimize overall");
        processIgnorablePhrase("but");
        processIgnorablePhrase("self pay portion");
        processIgnorablePhrase("self pay");
        processIgnorablePhrase("insurance portion");
        processIgnorablePhrase("instructions in spanish");
        processIgnorablePhrase("do not combine");
        processIgnorablePhrase("day time");
        processIgnorablePhrase("daw");
        processIgnoreablePhraseWithRegex("(brand)(.+)(necessary)");
        processIgnoreablePhraseWithRegex("(alternate)(.+)((oxycodone)|(hydrocodone))");
        processIgnoreablePhraseWithRegex("(brand)(.+)(only)");
        processIgnoreablePhraseWithRegex("(do not)(.+)(sedate)");
        processIgnoreablePhraseWithRegex("(do not)(.+)((on medication)|(on med))");
        processIgnoreablePhraseWithRegex("(prior)(.+)(skin care)");
        processIgnoreablePhraseWithRegex("((do not exceed)|(max))(.+)(acetaminophen)(.+)((24 hr)|(24hr)|(day))");
        processIgnoreablePhraseWithRegex("(dr)(.+)(to sign only)");
        processIgnoreablePhraseWithRegex("(please notify)(.+)(ready)");
        processIgnorablePhrase("hydrocodone");
        processIgnorablePhrase("roxycodone");
        processIgnorablePhrase("oxycodone");
        processIgnorablePhrase("brand");
        processIgnorablePhrase("inside cheek");

        // commands: take, apply (starts with)
        if (myLeftoverSig.startsWith("take")) {
            myCommand = "take";
            myLeftoverSig = myLeftoverSig.substring(4);
        } else if(myLeftoverSig.startsWith("tome")) {
            myCommand = "take";
            myLeftoverSig = myLeftoverSig.substring(4);
        } else if (myLeftoverSig.startsWith("to take")) {
            myCommand = "to take";
            myLeftoverSig = myLeftoverSig.substring(7);
        } else if (myLeftoverSig.startsWith("may take")) {
            myCommand = "may take";
            myLeftoverSig = myLeftoverSig.substring(8);
        } else if (myLeftoverSig.startsWith("take up to")) {
            myCommand = "take up to";
            myLeftoverSig = myLeftoverSig.substring(10);
        } else if (myLeftoverSig.startsWith("apply")) {
            myCommand = "apply";
            myLeftoverSig = myLeftoverSig.substring(5);
        } else if (myLeftoverSig.startsWith("to apply")) {
            myCommand = "to apply";
            myLeftoverSig = myLeftoverSig.substring(8);
        } else if (myLeftoverSig.startsWith("place")) {
            myCommand = "place";
            myLeftoverSig = myLeftoverSig.substring(5);
        } else if (myLeftoverSig.startsWith("to place")) {
            myCommand = "to place";
            myLeftoverSig = myLeftoverSig.substring(8);
        } else if (myLeftoverSig.startsWith("dissolve")) {
            myCommand = "dissolve";
            myLeftoverSig = myLeftoverSig.substring(8);
        } else if (myLeftoverSig.startsWith("to dissolve")) {
            myCommand = "to dissolve";
            myLeftoverSig = myLeftoverSig.substring(11);
        }

        // duration
        processDuration("for", "(day supply)|(day rx)|(day)", "d");
        processDuration("for", "(week supply)|(wk supply)|(week rx)|(wk rx)|(week)|(wk)", "wk");
        processDuration("for", "(month supply)|(mo supply)|(month rx)|(mo rx)|(month)|(mo)", "mo");
        processDuration("for up to", "(day supply)|(day rx)|(day)", "d");
        processDuration("for up to", "(week supply)|(wk supply)|(week rx)|(wk rx)|(week)|(wk)", "wk");
        processDuration("for up to", "(month supply)|(mo supply)|(month rx)|(mo rx)|(month)|(mo)", "mo");
        processDuration("up to", "(day supply)|(day rx)|(day)", "d");
        processDuration("up to", "(week supply)|(wk supply)|(week rx)|(wk rx)|(week)|(wk)", "wk");
        processDuration("up to", "(month supply)|(mo supply)|(month rx)|(mo rx)|(month)|(mo)", "mo");
        processDuration("each rx should last for", "(day)", "d");
        processDuration("each rx should last for", "(week)|(wk)", "wk");
        processDuration("each rx should last for", "(month)|(mo)", "mo");
        processDuration("must last at least to last", "(day)", "d");
        processDuration("must last at least to last", "(week)|(wk)", "wk");
        processDuration("must last at least to last", "(month)|(mo)", "mo");
        processDuration("must last at least", "(day)", "d");
        processDuration("must last at least", "(week)|(wk)", "wk");
        processDuration("must last at least", "(month)|(mo)", "mo");
        processDuration("each fill must last", "(day)", "d");
        processDuration("each fill must last", "(week)|(wk)", "wk");
        processDuration("each fill must last", "(month)|(mo)", "mo");
        processDuration("must last", "(day)", "d");
        processDuration("must last", "(week)|(wk)", "wk");
        processDuration("must last", "(month)|(mo)", "mo");
        processDuration("must las", "(day)", "d");
        processDuration("must las", "(week)|(wk)", "wk");
        processDuration("must las", "(month)|(mo)", "mo");
        processDuration("to last at least", "(day)", "d");
        processDuration("to last at least", "(week)|(wk)", "wk");
        processDuration("to last at least", "(month)|(mo)", "mo");
        processDuration("to last", "(day)", "d");
        processDuration("to last", "(week)|(wk)", "wk");
        processDuration("to last", "(month)|(mo)", "mo");
        processDuration("", "(day supply)|(day fill)|(day rx)", "d");
        processDuration("", "(week supply)|(wk supply)|(week fill)|(wk fill)|(week rx)|(wk rx)", "wk");
        processDuration("", "(month supply)|(mo supply)|(month fill)|(mo fill)(month rx)|(mo rx)", "mo");

        // daily max
        processDailyMax("limit up to", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyMax("for max", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyMax("for maximum", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyMax("up to max", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyMax("up to maximum", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyMax("max", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyMax("maximum", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyMax("do not exceed", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyMax("not to exceed", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyMax("to exceed", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyMax("for up to", "(every day)|(in every day)|(/day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)"); // removed (day)
        processDailyMax("up to", "(every day)|(in every day)|(/day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)"); // removed (day)
        processDailyMax("upto", "(every day)|(in every day)|(/day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)"); // removed (day)

        // daily ave
        processDailyAve("up to avg", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyAve("for avg", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyAve("avg", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyAve("up to ave", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyAve("for ave", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyAve("ave", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyAve("up to average", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyAve("for average", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyAve("average", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyAve("total", "(every day)|(in every day)|(/day)|(day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyAve("for", "(every day)|(/day)|(24 hr)|(24hr)|(in 24hr)|(in 24 hr)|(per 24hr day)");
        processDailyAve("\\(", "(every day\\))");
        processDailyAve("or", "(every day)");

        // process route
        myLeftoverSig = getCleanedStr(myLeftoverSig);
        processRoute("po");
        processRoute("sl");
        processRoute("skin");
        processRoute("feeding tube");

        // process dosing
        myLeftoverSig = getCleanedStr(myLeftoverSig);
        processDosing();

        // process interval
        myLeftoverSig = getCleanedStr(myLeftoverSig);
        ParsedInterval parsedInterval = IntervalParser.fromLeftoverSig(myLeftoverSig);
        if (parsedInterval != null) {
            myLeftoverSig = myLeftoverSig.replaceAll(parsedInterval.getRegexPattern(), "");
            myIntervalLow = parsedInterval.getIntervalLow();
            myIntervalHigh = parsedInterval.getIntervalHigh();
            myIntervalTimeUnits = parsedInterval.getIntervalTimeUnits();
            if (parsedInterval.getPrn() != null) {
                myIsPrn = parsedInterval.getPrn();
            }
        }

        // process frequency
        myLeftoverSig = getCleanedStr(myLeftoverSig);
        ParsedFrequency parsedFrequency = FrequencyParser.fromLeftoverSig(myLeftoverSig);
        if (parsedFrequency != null) {
            myLeftoverSig = myLeftoverSig.replaceAll(parsedFrequency.getRegexPattern(), "");
            myFrequencyLow = parsedFrequency.getFrequencyLow();
            myFrequencyHigh = parsedFrequency.getFrequencyHigh();
            myFrequencyTimeUnits = parsedFrequency.getFrequencyTimeUnits();
            if (parsedFrequency.getPrn() != null) {
                myIsPrn = parsedFrequency.getPrn();
            }
        } else {
            String abbr = TimingAbbreviationParser.getTimingAbbreviationCode(myNormalizedSig);
            TimingAbbreviationParser.FreqAbbrInfo dailyFrequency = TimingAbbreviationParser.resolveDailyFrequency(abbr);
            if (dailyFrequency != null) {
                myFrequencyLow = dailyFrequency.low();
                myFrequencyHigh = dailyFrequency.high();
                myFrequencyTimeUnits = dailyFrequency.units();
            }
        }

        myLeftoverSig = getCleanedStr(myLeftoverSig);

        // prn
        processPrnReasonWithRegex("(prn)([for]?)(.*)((pain)|(pain control))", "pain");
        processPrnReason("cough and congestion");
        processPrnReason("headaches");
        processPrnReason("headache");
        processPrnReason("nausea and stomach cramps");
        processPrnReasonWithRegex("(prn)([for]?)(.*)(cough)", "cough");
        processPrnReason("restless leg syndrome");
        processPrnReason("restless legs");
        processPrnReason("rls");
        processPrnReason("opioid dependence");

        // indication not part of prn
        processIndicationWithRegex("(for)(.*)((pain)|(pain control))", "pain");
        processIndicationWithRegex("(indication)(.*)((pain)|(pain control))", "pain");
        processIndication("cough and congestion");
        processIndication("cough");
        processIndication("headaches");
        processIndication("headache");
        processIndication("nausea and stomach cramps");
        processIndication("restless leg syndrome");
        processIndication("restless legs");
        processIndication("rls");
        processIndication("opioid dependence");

        myLeftoverSig = getCleanedStr(myLeftoverSig);


        if (myLeftoverSig.endsWith(" prn")) {
            myIsPrn = true;
            myLeftoverSig = myLeftoverSig.substring(0, myLeftoverSig.length() - 4);
        } else if (myLeftoverSig.endsWith("prn")) {
            myIsPrn = true;
            myLeftoverSig = myLeftoverSig.substring(0, myLeftoverSig.length() - 3);
        }

        // cleanup
        if (myLeftoverSig.equals(")")) {
            myLeftoverSig = "";
        } else if (myLeftoverSig.equals("(")) {
            myLeftoverSig = "";
        } else if (myLeftoverSig.equals("prn (")) {
            myIsPrn = true;
            myLeftoverSig = "";
        }
    }

    public String getTimingAbbreviationCode() {
        return TimingAbbreviationParser.getTimingAbbreviationCode(myNormalizedSig);
    }

    private void processRoute(String route) {
        if (myLeftoverSig.contains(" " + route + " ")) {
            myRoute = route;
            myLeftoverSig = myLeftoverSig.replaceAll(" " + route + " ", " ");
        }

        if (myLeftoverSig.startsWith(route + " ")) {
            myRoute = route;
            myLeftoverSig = myLeftoverSig.replaceAll("(^" + route + " )", "");
        }

        if (myLeftoverSig.endsWith(" " + route)) {
            myRoute = route;
            myLeftoverSig = myLeftoverSig.replaceAll("( " + route + "$)", "");
        }
    }

    private void processDosing() {
        // assume dose starts at beginning at this point

        // route portion is now redundant with processRoute --> that was moved away because some sigs had routes in different location

        // group              1              2                           6                 11                                       12                               23              24            28                 29
        //                           3       4      5       7    8      9       10                13      14     15       16     17     18   19    20  21    22               25    26   27              30    31   32       33
        // example                   1      (.)    (5)               (-2(.)(5))            (_)         (tab/pill/capsule/patch/film/mg/mcg/ml/cc/tsp)                (_)      ([alt dose])        (_)      (po/sl/skin/feeding tube)
        String patternStr = "(^)(([\\d]+)([\\.]?)([\\d]*))((-)([\\d]+)([\\.]?)([\\d]*))?([\\s]?)((tab)|(pill)|(capsule)|(patch)|(film)|(mg)|(mcg)|(ml)|(cc)|(tsp))?([\\s]?)(([\\(])(.*)([\\)]))?([\\s]?)((po)|(sl)|(skin)|(feeding tube))?";
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(myLeftoverSig);
        if (matcher.find()) {
            String doseLow = matcher.group(2);
            String doseHigh = matcher.group(6);
            String doseUnits = matcher.group(12);
            String altDoseSig = matcher.group(24);
            String route = matcher.group(29);

            try {
                myDoseLow = Double.valueOf(doseLow);
                if ((doseHigh != null) && (!doseHigh.equals(""))) {
                    myDoseHigh = Double.valueOf(doseHigh.substring(1)); // remove the starting '-'
                } else {
                    myDoseHigh = myDoseLow;
                }

                if ((doseUnits != null) && (!doseUnits.equals(""))) {
                    myDoseUnits = doseUnits;
                }

                if ((route != null) && (!route.equals(""))) {
                    myRoute = route;
                }
            } catch (Exception e) {
                System.err.println("> Unable to convert " + myLeftoverSig + " to a dose form");
            }

            if ((altDoseSig != null) && (!altDoseSig.equals(""))) {
                String altDosePatternStr = "(\\()(([\\d]+)([\\.]?)([\\d]*))((-)([\\d]+)([\\.]?)([\\d]*))?([\\s]?)((tab)|(pill)|(capsule)|(patch)|(film)|(mg)|(mcg)|(ml)|(cc)|(tsp))?(\\))";
                Pattern altDosePattern = Pattern.compile(altDosePatternStr);
                Matcher altDoseMatcher = altDosePattern.matcher(altDoseSig);

                if (altDoseMatcher.find()) {
                    String altDoseLow = altDoseMatcher.group(2);
                    String altDoseHigh = altDoseMatcher.group(6);
                    String altDoseUnits = altDoseMatcher.group(12);

                    try {
                        myAlternateDoseLow = Double.valueOf(altDoseLow);
                        if ((altDoseHigh != null) && (!altDoseHigh.equals(""))) {
                            myAlternateDoseHigh = Double.valueOf(altDoseHigh.substring(1));
                        } else {
                            myAlternateDoseHigh = myAlternateDoseLow;
                        }

                        if ((altDoseUnits != null) && (!altDoseUnits.equals(""))) {
                            myAlternateDoseUnits = altDoseUnits;
                        }
                    } catch (Exception e) {
                        System.err.println("> Unable to convert " + myLeftoverSig + " to alternate dose form");
                    }
                }
            }
        }
        myLeftoverSig = myLeftoverSig.replaceAll(patternStr, "");
    }

    private void processDailyMax(String prefix, String suffixRegex) {
        String patternStr = "(" + prefix + ")(\\s?)([#]?)(\\s?)([\\d]+)(\\s?)(x)(\\s?)(" + suffixRegex + ")";
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(myLeftoverSig);

        if (matcher.find()) {
            try {
                myMaxDailyFrequency = Integer.valueOf(matcher.group(5));
                myLeftoverSig = myLeftoverSig.replaceAll(patternStr, "");
            } catch (Exception e) {
                System.err.println("> Unable to convert " + myLeftoverSig + " to a daily max frequency");
            }
        } else {
            // group             1          2     3       4             5               9                      10
            //                                                  6       7      8               11+
            // example        prefix       (_)   (#)     (_)       1   (.)    (5)      (_)    (tab/pill/capsule/patch/film/mg/mcg/ml/cc/tsp)
            patternStr = "(" + prefix + ")(\\s?)([#]?)(\\s?)(([\\d]+)([\\.]?)([\\d]*))([\\s]?)((tab)|(pill)|(capsule)|(patch)|(film)|(mg)|(mcg)|(ml)|(cc)|(tsp))?([\\s]?)(" + suffixRegex + ")";
            pattern = Pattern.compile(patternStr);
            matcher = pattern.matcher(myLeftoverSig);

            if (matcher.find()) {
                try {
                    myMaxDailyDose = Double.valueOf(matcher.group(5));
                    if (StringUtil.notNullOrEmpty(matcher.group(10))) {
                        myMaxDailyDoseUnits = matcher.group(10);
                    }
                    myLeftoverSig = myLeftoverSig.replaceAll(patternStr, "");
                } catch (Exception e) {
                    System.err.println("> Unable to convert " + myLeftoverSig + " to a daily max dose");
                }
            }
        }
    }

    private void processDailyAve(String prefix, String suffixRegex) {
        String patternStr = "(" + prefix + ")(\\s?)([#]?)(\\s?)([\\d]+)(\\s?)(x)(\\s?)(" + suffixRegex + ")";
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(myLeftoverSig);

        if (matcher.find()) {
            try {
                myAverageDailyFrequency = Integer.valueOf(matcher.group(5));
                myLeftoverSig = myLeftoverSig.replaceAll(patternStr, "");
            } catch (Exception e) {
                System.err.println("> Unable to convert " + myLeftoverSig + " to a daily average frequency");
            }
        } else {
            // group             1          2     3       4             5               9                      10
            //                                                  6       7      8               11+
            // example        prefix       (_)   (#)     (_)       1   (.)    (5)      (_)    (tab/pill/capsule/patch/film/mg/mcg/ml/cc/tsp)
            patternStr = "(" + prefix + ")(\\s?)([#]?)(\\s?)(([\\d]+)([\\.]?)([\\d]*))([\\s]?)((tab)|(pill)|(capsule)|(patch)|(film)|(mg)|(mcg)|(ml)|(cc)|(tsp))?([\\s]?)(" + suffixRegex + ")";
            pattern = Pattern.compile(patternStr);
            matcher = pattern.matcher(myLeftoverSig);

            if (matcher.find()) {
                try {
                    myAverageDailyDose = Double.valueOf(matcher.group(5));
                    if (StringUtil.notNullOrEmpty(matcher.group(10))) {
                        myAverageDailyDoseUnits = matcher.group(10);
                    }
                    myLeftoverSig = myLeftoverSig.replaceAll(patternStr, "");
                } catch (Exception e) {
                    System.err.println("> Unable to convert " + myLeftoverSig + " to a daily average dose");
                }
            }
        }
    }

    private void processDuration(String prefix, String suffix_durationUnitsRegex, String durationUnits) {
        String patternStr = "(" + prefix + ")(\\s*)([\\d]+)(\\s?)(" + suffix_durationUnitsRegex + ")";
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(myLeftoverSig);

        int matchCount = 0;
        Integer duration = null;

        if (myDurationAlreadyParsed == false) {
            while (matcher.find()) {
                matchCount++;
                try {
                    duration = Integer.valueOf(matcher.group(3));
                } catch (Exception e) {
                    System.err.println("> Unable to convert " + myLeftoverSig + " to a duration");
                }
            }

            if (matchCount == 1) // avoid processing cases where there are multiple patterns, e.g., take 1 tab for x days, 1/5 tab for y days
            {
                myDuration = duration;
                myDurationTimeUnits = durationUnits;
                myLeftoverSig = myLeftoverSig.replaceAll(patternStr, "");
                myDurationAlreadyParsed = true;
            }
        }
    }

    private void processEarliestFillDate(String earliestFillDatePrefix) {
        String patternStr = "(" + earliestFillDatePrefix + " )([\\d]+/[\\d]+/[\\d]+)";
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(myNormalizedSig);
        if (matcher.find()) {
            String dateStr = matcher.group(2);
            try {
                myEarliestFillDate = dateUtility.getDateFromString(dateStr, "MM/dd/yy");

            } catch (Exception e) {
                System.err.println("> Unable to convert " + dateStr + " to a date");
            }
        }
        myNormalizedSig = myNormalizedSig.replaceAll(patternStr, "");
    }

    private void processUseDates(String useDatesPrefix) {
        String patternStr = "(" + useDatesPrefix + " )([\\d]+/[\\d]+)(/?)([\\d]*)(\\s*)(-)(\\s*)([\\d]+/[\\d]+/[\\d]+)";
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(myNormalizedSig);

        if (matcher.find()) {
            myNormalizedSig = myNormalizedSig.substring(0, matcher.start()) + myNormalizedSig.substring(matcher.end());

            String endDateStr = matcher.group(8);
            try {
                myEndDate = dateUtility.getDateFromString(endDateStr, "MM/dd/yy");
            } catch (Exception e) {
                System.err.println("> Unable to convert " + endDateStr + " to a date");
            }

            String startDateStr = matcher.group(2);
            if (matcher.group(4).length() > 0) {
                startDateStr += "/" + matcher.group(4);
            } else {
                startDateStr += "/" + dateUtility.getDateAsString(myEndDate, "yyyy");
            }

            try {
                myStartDate = dateUtility.getDateFromString(startDateStr, "MM/dd/yy");
                if (myStartDate.after(myEndDate)) {
                    myStartDate = dateUtility.getDateAfterAddingTime(myStartDate, Calendar.YEAR, -1);
                }
            } catch (Exception e) {
                System.err.println("> Unable to convert " + startDateStr + " to a date");
            }
        }
        myNormalizedSig = myNormalizedSig.replaceAll(patternStr, "");
    }

    private void processIgnorablePhrase(String ignorablePhrase) {
        if (myLeftoverSig.contains(ignorablePhrase)) {
            if (myIgnorableSig == null) {
                myIgnorableSig = ignorablePhrase;
            } else {
                myIgnorableSig += "; " + ignorablePhrase;
            }

            myLeftoverSig = myLeftoverSig.replaceAll(ignorablePhrase, "");
        }
    }

    private void processIgnoreablePhraseWithRegex(String ignorableRegex) {
        Pattern pattern = Pattern.compile(ignorableRegex);
        Matcher matcher = pattern.matcher(myLeftoverSig);

        if (matcher.find()) {
            if (myIgnorableSig == null) {
                myIgnorableSig = myLeftoverSig.substring(matcher.start(), matcher.end());
            } else {
                myIgnorableSig += "; " + myLeftoverSig.substring(matcher.start(), matcher.end());
            }
        }
        myLeftoverSig = myLeftoverSig.replaceAll(ignorableRegex, "");
    }

    private void processPrnReasonWithRegex(String prnReasonRegex, String prnReasonLabel) {
        Pattern pattern = Pattern.compile(prnReasonRegex);
        Matcher matcher = pattern.matcher(myLeftoverSig);

        if (matcher.find()) {
            myIsPrn = true;
            myIndication = prnReasonLabel;
        }
        myLeftoverSig = myLeftoverSig.replaceAll(prnReasonRegex, "");
    }

    private void processIndicationWithRegex(String indicationRegex, String indicationLabel) {
        Pattern pattern = Pattern.compile(indicationRegex);
        Matcher matcher = pattern.matcher(myLeftoverSig);

        if (matcher.find()) {
            myIndication = indicationLabel;
        }
        myLeftoverSig = myLeftoverSig.replaceAll(indicationRegex, "");
    }

    private void processPrnReason(String prnReason) {
        if (myLeftoverSig.contains("prn for " + prnReason)) {
            myIsPrn = true;
            myIndication = prnReason;
            myLeftoverSig = myLeftoverSig.replace("prn for " + prnReason, "");
        } else if (myLeftoverSig.contains("prn " + prnReason)) {
            myIsPrn = true;
            myIndication = prnReason;
            myLeftoverSig = myLeftoverSig.replace("prn " + prnReason, "");
        }
    }

    private void processIndication(String indication) {
        if (myLeftoverSig.contains("for " + indication)) {
            myIndication = indication;
            myLeftoverSig = myLeftoverSig.replace("for " + indication, "");
        } else if (myLeftoverSig.contains("indications " + indication)) {
            myIndication = indication;
            myLeftoverSig = myLeftoverSig.replace("indications " + indication, "");
        } else if (myLeftoverSig.contains("indication " + indication)) {
            myIndication = indication;
            myLeftoverSig = myLeftoverSig.replace("indication " + indication, "");
        }
    }

    /**
     * Intended to be called iteratively after other processing.  Contains methods for cleaning issues with strings that may arise repetitively.
     *
     * @param strUnchangedByFunction
     * @return
     */
    private String getCleanedStr(String strUnchangedByFunction) {
        String strToReturn = strUnchangedByFunction;

        // convert all multi-space spaces to single space
        while (strToReturn.contains("  ")) {
            strToReturn = strToReturn.replace("  ", " ");
        }

        // remove all leading periods and spaces
        while (strToReturn.startsWith(".")) {
            strToReturn = strToReturn.substring(1);
        }

        while (strToReturn.startsWith(" ")) {
            strToReturn = strToReturn.substring(1);
        }

        // remove periods following a non-number character
        strToReturn = strToReturn.replaceAll("([\\D])([.])", "$1");

        // remove hyphens following a non-number character (unless it's a space or d, e.g., bid-tid)
        strToReturn = strToReturn.replaceAll("([\\D&&[^\\s]&&[^d]])([-])", "$1");

        // remove ending periods and hyphens
        while (strToReturn.endsWith(".") || strToReturn.endsWith("-")) {
            strToReturn = strToReturn.substring(0, strToReturn.length() - 1);
        }

        while (strToReturn.endsWith(". ") || strToReturn.endsWith("- ")) {
            strToReturn = strToReturn.substring(0, strToReturn.length() - 2);
        }

        // remove ending spaces
        while (strToReturn.endsWith(" ")) {
            strToReturn = strToReturn.substring(0, strToReturn.length() - 1);
        }

        strToReturn = strToReturn.replaceAll("(" + Pattern.quote("(") + ")(\\s*)(" + Pattern.quote(")") + ")", "");

        return strToReturn;
    }

    public static void main(String[] args) {
        RxSig sig = new RxSig("Apply 20 mcg weekly");
        System.out.println(sig.getMaxDailyDose());

        //DecimalFormat myDecimalFormatter = new DecimalFormat("#########.#");
        //System.out.println(myDecimalFormatter.format(31.151));
    }
}

