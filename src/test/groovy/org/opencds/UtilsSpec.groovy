package org.opencds

import org.hl7.fhir.dstu2.model.MedicationOrder
import org.hl7.fhir.dstu2.model.MedicationOrder.MedicationOrderDosageInstructionComponent
import org.hl7.fhir.dstu2.model.SimpleQuantity

import spock.lang.Specification

class UtilsSpec extends Specification {

    def 'test isMedicationOrderMissingDoseQuantity; medOrder has dose'() {
        given:
        MedicationOrder medOrder = new MedicationOrder()
        medOrder.getDosageInstruction().add(new MedicationOrderDosageInstructionComponent().setDose(new SimpleQuantity().setValue(1.0)))
        
        when:
        boolean doseMissing = Utils.isMedicationOrderMissingDoseQuantity(medOrder)
        
        then:
        !doseMissing
    }
    
    def 'test isMedicationOrderMissingDoseQuantity; medOrder does not have a dose'() {
        given:
        MedicationOrder medOrder = new MedicationOrder()
        
        when:
        boolean doseMissing = Utils.isMedicationOrderMissingDoseQuantity(medOrder)
        
        then:
        doseMissing
    }

}
